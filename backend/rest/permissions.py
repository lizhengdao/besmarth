from rest_framework.permissions import BasePermission


class AccountViewPermission(BasePermission):
    """
    POST is allowed by everyone,
    all others verbs are only allowed vor authenticated user
    """

    def has_permission(self, request, view):
        return bool(
            request.method == 'POST' or
            request.user and
            request.user.is_authenticated
        )
