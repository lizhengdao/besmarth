import os
from io import BytesIO

import qrcode
from PIL import ImageDraw, Image
from django.conf import settings
from django.http import HttpResponse
from rest_framework import generics, authentication
from rest_framework.permissions import IsAuthenticated


class AccountCodeView(generics.RetrieveAPIView):
    """
    Returns a PNG image containing the QR code with the username of the account
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        # build a qr code
        qr = qrcode.QRCode(
            version=10,
            error_correction=qrcode.constants.ERROR_CORRECT_H,
            box_size=15,
            border=1,
        )
        qr.add_data(request.user.username)
        qr.make(fit=False)
        img = qr.make_image(fill_color="black", back_color="rgb(242, 242, 242)")
        # convert to colored image with transparency
        img = img.convert('RGBA')
        width, height = img.size

        # load besmarth logo
        icon_img = Image.open(os.path.join(settings.STATIC_ROOT, "icon.png"))
        icon_img = icon_img.convert('RGBA')
        icon_width, icon_height = icon_img.size
        # calculate position of icon
        icon_x = int(width / 2 - (icon_width / 2))
        icon_y = int(height / 2 - (icon_width / 2))

        # paint a rectangle in the middle because the logo is transparent
        draw = ImageDraw.Draw(img)
        draw.rectangle([icon_x, icon_y, icon_x + icon_width, icon_y + icon_height], fill="rgb(242, 242, 242)")
        # paste the icon into the qr code
        img.paste(icon_img, (icon_x, icon_y), icon_img)

        # return the qr code as image to the client
        temp = BytesIO()
        img.save(temp, format="png")
        return HttpResponse(temp.getvalue(), content_type="image/png")
