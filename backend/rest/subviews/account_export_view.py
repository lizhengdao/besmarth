from django.db.models import Q
from requests import Response
from rest_framework import generics, authentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from ..models import ChallengeParticipation, Friendship
from ..serializers import MinimalFriendshipSerializer, ChallengeParticipationSerializer, \
    AccountExportSerializer


class AccountExportView(generics.RetrieveAPIView):
    """
    get:
        Returns all data associated with the current account as JSON (gdpr data export).
    """
    serializer_class = AccountExportSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        friendships = Friendship.objects.filter(Q(receiver=request.user) | Q(sender=request.user)).all()
        participations = ChallengeParticipation.objects.filter(user=request.user)

        data = {
            "account": AccountExportSerializer(request.user, context={'request': request}).data,
            "friendships": MinimalFriendshipSerializer(friendships, many=True, context={'request': request}).data,
            "participations": ChallengeParticipationSerializer(participations, many=True,
                                                               context={'request': request}).data}
        return Response(data)
