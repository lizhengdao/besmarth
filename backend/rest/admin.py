from django.contrib import admin

from .models import Tip, TipSchedule, Challenge, Topic, Account, Reward, AchievedReward, TopicLevel, Diary, Hypothesis, DailyMonitoring, WeeklyMonitoring, FinalMonitoring, Post, Factor, ChallengeParticipation, UserChallenge, SelfCommitment

# Register your models here.

admin.site.register(Tip)
admin.site.register(TipSchedule)
admin.site.register(Challenge)
admin.site.register(Account)
admin.site.register(Topic)
admin.site.register(Reward)
admin.site.register(AchievedReward)
admin.site.register(TopicLevel)
admin.site.register(Diary)
admin.site.register(Hypothesis)
admin.site.register(DailyMonitoring)
admin.site.register(WeeklyMonitoring)
admin.site.register(FinalMonitoring)
admin.site.register(Post)
admin.site.register(Factor)
admin.site.register(ChallengeParticipation)
admin.site.register(UserChallenge)
admin.site.register(SelfCommitment)
