from enum import Enum
from django.db.models import Q
from ..models import ChallengeParticipation, UserChallenge, AchievedReward, Reward, Topic
from django.db.models import Count
from collections import Counter

class ChangeType(Enum):
    CHALLENGE = 1
    OTHER = 9


class Helper:
    new_rewards = []

    def change(self, type, user):
        if type == ChangeType.CHALLENGE:
            return self.__check_challenges(user)

        elif type == ChangeType.OTHER:
            return -1
        else:
            return -1

    def __check_challenges(self, user):
        # Load participation for the user with amount
        participations = ChallengeParticipation.objects.filter(
            Q(user=user)).values('challenge').annotate(total=Count('progress'))

        all_challenges = UserChallenge.objects.all()
        rewards = Reward.objects.all()
        achieved_rewards = AchievedReward.objects.filter(user=user)

        completed_challenges = []
        # Get all challenges which are done by the user
        for participation in participations:
            c = participation.get('challenge')
            if c is None:
                continue

            challenge = all_challenges.get(pk=participation.get('challenge'))
            if participation.get('total') >= challenge.duration:
                completed_challenges.append(challenge)

        # remove already existing rewards
        for r in achieved_rewards:
            rewards = rewards.exclude(pk=r.reward.pk)

        # compare rewards with amount of challenges
        for possible_new_reward in rewards:
            if possible_new_reward.type == "TROPHY":
                self.__trophy_reward(possible_new_reward, completed_challenges)

            elif possible_new_reward.type == "MEDAL":
                self.__medal_reward(possible_new_reward, completed_challenges)

        self.__add_to_database(user)
        return len(self.new_rewards) > 0

    def get_header(self):
        if len(self.new_rewards) > 0:
            head = []
            for r in self.new_rewards:
                head.append(r.pk)

            self.new_rewards.clear()
            return head
        else:
            return None

    # check if in all topics enough challenges were made
    def __trophy_reward(self, reward, completed_challenges):
        challenges_to_be_done = reward.condition
        count_topic = Topic.objects.all().count()
        result = Counter(getattr(c, 'topic_id') for c in completed_challenges)
        achieved = True
        for r in result.values():
            if r < challenges_to_be_done:
                achieved = False
                break

        if achieved and count_topic <= len(result):
            self.new_rewards.append(reward)

    # check if in one topic enough challenges were made
    def __medal_reward(self, reward, completed_challenges):
        challenges_to_be_done = reward.condition
        result = Counter(getattr(c, 'topic_id') for c in completed_challenges)
        for r in result.values():
            if r >= challenges_to_be_done:
                self.new_rewards.append(reward)
                break

    # adds the new rewards to the database
    def __add_to_database(self, user):
        for reward in self.new_rewards:
            if not AchievedReward.objects.filter(reward=reward, user=user).exists(): # sometimes do different request add at the same time
                r = AchievedReward(reward=reward, user=user)
                r.save()
