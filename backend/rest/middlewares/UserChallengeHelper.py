import json
from ..serializers import UserChallengeSerializer
from ..models import Topic, SelfCommitment, Account


class UserChallengeHelper:
    def create_user_challenge(self, request):
        challenge_data = getattr(request, '_body', request.body)
        challenge_data = json.loads(challenge_data)
        print(challenge_data)
        icon = ''
        challenge_data['icon'] = icon
        mediaIndex = challenge_data['image'].find('/media/') + 7
        image = (challenge_data['image'])[mediaIndex:]
        challenge_data['image'] = image
        topic = Topic.objects.get(topic_name=challenge_data['topic'])
        challenge_data['topic'] = topic
        selfCommitment = SelfCommitment.objects.get(self_commitment1=challenge_data['selfCommitment']['self_commitment1'])
        challenge_data['selfCommitment'] = selfCommitment
        challenge_data['internal_id'] = challenge_data['id']
        id = int(str(challenge_data['user']) + str(challenge_data['id']))
        challenge_data['id'] = id
        user = Account.objects.get(pk=challenge_data['user'])
        challenge_data['user'] = user
        UserChallengeSerializer.create(UserChallengeSerializer, challenge_data)
