from .Helper import Helper, ChangeType
from .UserChallengeHelper import UserChallengeHelper


class RewardMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        # check for new reward
        if (request.method == "POST" or request.method == "PUT") and str(request.path).find(
                "challenge") > -1 and not request.user.is_anonymous:
            helper = Helper()
            if helper.change(ChangeType.CHALLENGE, request.user):
                response.__setitem__('X-Achieved-Reward', helper.get_header())

        return response


class UserChallengeMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Code to be executed for each request before the view (and later middleware) are called.
        if (request.method == "POST") and str(request.path).find("participation") > -1 and not str(request.path).find("progress") > -1:
            user_challenge_helper = UserChallengeHelper()
            user_challenge_helper.create_user_challenge(request)

        response = self.get_response(request)
        return response
