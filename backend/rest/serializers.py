from django.contrib.auth.hashers import make_password
from rest_framework import serializers

from .models import Tip, Challenge, Account, Topic, ChallengeParticipation, ChallengeProgress, FriendRequest, Reward, \
    AchievedReward, Friendship, Diary, Hypothesis, DailyMonitoring, WeeklyMonitoring, FinalMonitoring, Post, Factor, UserChallenge, SelfCommitment


class CreateAccountSerializer(serializers.Serializer):
    """ Serializer for POST /account  most fields from account are not needed, therefore this is not a ModelSerializer"""
    username = serializers.CharField(required=False)
    password = serializers.CharField(required=False)
    pseudonymous = serializers.BooleanField(default=False, required=False)
    data_usage = serializers.BooleanField(default=False, required=False)

    def validate(self, attrs):
        pseudonymous = attrs.get('pseudonymous')
        username = attrs.get('username')
        password = attrs.get('password')

        if pseudonymous or (username and password):
            pass
        else:
            raise serializers.ValidationError('Must include either "pseudonymous" or "username" and "password".')
        return attrs


class AccountSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'username', 'email', 'password', 'first_name', 'pseudonymous', 'data_usage']

    @staticmethod
    def validate_password(value: str) -> str:
        return make_password(value)

    def to_representation(self, obj):
        ret = super(AccountSerializer, self).to_representation(obj)
        # do not output password
        ret.pop('password')
        return ret


class TipSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tip
        fields = ['id', 'title', 'description', 'image']


class TopicSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Topic
        fields = ['id', 'internal_id', 'topic_name', 'topic_description', 'image_level1', 'image_level2', 'image_level3',
                  'image_level4', 'image_level5']


class SelfCommitmentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SelfCommitment
        fields = ['self_commitment1', 'self_commitment2', 'self_commitment3', 'self_commitment4', 'self_commitment5']


class ChallengeSerializer(serializers.HyperlinkedModelSerializer):
    topic = serializers.SlugRelatedField(
        many=False,
        slug_field='topic_name',
        queryset=Topic.objects.all()
    )

    selfCommitment = SelfCommitmentSerializer(many=False)

    class Meta:
        model = Challenge
        fields = ['id', 'title', 'description', 'icon', 'image',
                  'duration', 'color', 'difficulty', 'periodicity', 'topic',
                  'category', 'selfCommitment']

    def to_representation(self, obj):
        ret = super(ChallengeSerializer, self).to_representation(obj)
        ret['image'] = self.context['request'].build_absolute_uri(ret['image'])
        ret['icon'] = self.context['request'].build_absolute_uri(ret['icon'])
        return ret

    def create(self, validated_data):
        image = "images/challenges/standard-image-challenge.jpg"
        selfCommitment_data = validated_data.pop('selfCommitment')
        selfCommitment = SelfCommitment.objects.create(**selfCommitment_data)
        challenge = Challenge.objects.create(**validated_data, image=image, selfCommitment=selfCommitment)
        return challenge


class UserChallengeSerializer(serializers.HyperlinkedModelSerializer):
    topic = serializers.SlugRelatedField(
        many=False,
        slug_field='topic_name',
        queryset=Topic.objects.all()
    )

    selfCommitment = SelfCommitmentSerializer(many=False)
    user = AccountSerializer(many=False, read_only=True)

    class Meta:
        model = UserChallenge
        fields = ['id', 'internal_id', 'title', 'description', 'icon', 'image',
                  'duration', 'color', 'difficulty', 'periodicity', 'topic',
                  'category', 'selfCommitment', 'user']

    def to_representation(self, obj):
        ret = super(UserChallengeSerializer, self).to_representation(obj)
        ret['image'] = self.context['request'].build_absolute_uri(ret['image'])
        ret['icon'] = self.context['request'].build_absolute_uri(ret['icon'])
        return ret

    def create(self, validated_data):
        challenge = UserChallenge.objects.create(**validated_data)
        return challenge


class ChallengeProgressSerializer(serializers.HyperlinkedModelSerializer):
    participation = serializers.PrimaryKeyRelatedField(many=False, read_only=True)

    class Meta:
        model = ChallengeProgress
        fields = ['id', 'create_time', 'participation', 'mark_deleted']


class ChallengeParticipationSerializer(serializers.HyperlinkedModelSerializer):
    challenge = UserChallengeSerializer(many=False, read_only=True)
    user = AccountSerializer(many=False, read_only=True)
    progress = ChallengeProgressSerializer(many=True)
    progress_loggable = serializers.ReadOnlyField()

    class Meta:
        model = ChallengeParticipation
        fields = ['id', 'joined_time', 'challenge', 'user', 'progress', 'shared', 'mark_deleted', 'progress_loggable']


class FriendshipRequestSerializer(serializers.ModelSerializer):
    receiver = AccountSerializer()
    sender = AccountSerializer()

    class Meta:
        model = FriendRequest
        fields = ['id', 'receiver', 'sender', 'date']


class RewardSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Reward
        fields = ['id', 'type', 'color', 'points', 'condition']


class AchievedRewardSerializer(serializers.HyperlinkedModelSerializer):
    reward = RewardSerializer()

    class Meta:
        model = AchievedReward
        fields = ['reward']


class AccountExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'pseudonymous', 'is_active', 'date_joined',
                  'last_login']


class MinimalAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'username', 'first_name']


class MinimalFriendshipSerializer(serializers.ModelSerializer):
    receiver = MinimalAccountSerializer()
    sender = MinimalAccountSerializer()

    class Meta:
        model = Friendship
        fields = ['receiver', 'sender', 'date']


class HypothesisSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Hypothesis
        fields = ['id', 'startHypothesis', 'endHypothesis', 'difficulty', 'limitation', 'environment', 'contribution', 'burden']


class DailyMonitoringSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DailyMonitoring
        fields = ['id', 'wellbeing', 'done']


class WeeklyMonitoringSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WeeklyMonitoring
        fields = ['id', 'week', 'self_action', 'socialEnvironment_action', 'political_action', 'producer_action']


class FinalMonitoringSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FinalMonitoring
        fields = ['id']


class PostSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Post
        fields = ['id', 'text', 'positive', 'week']


class FactorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Factor
        fields = ['id', 'text', 'positive', 'finalFactor']


class DiarySerializer(serializers.HyperlinkedModelSerializer):
    user = AccountSerializer(many=False, required=False, help_text="Linked user")
    challenge = UserChallengeSerializer(many=False, required=False, help_text="Linked challenge")
    hypothesises = HypothesisSerializer(many=True, required=False, allow_null=False, help_text="Linked hypothesises")
    daily_monitorings = DailyMonitoringSerializer(many=True, required=False, help_text="Linked daily monitorings")
    weekly_monitorings = WeeklyMonitoringSerializer(many=True, required=False, help_text="Linked weekly monitorings")
    final_monitoring = FinalMonitoringSerializer(many=False, required=False, help_text="Linked final monitorings")
    posts = PostSerializer(many=True, required=False, help_text="Linked posts")
    factors = FactorSerializer(many=True, required=False, help_text="Linked factors")

    class Meta:
        model = Diary
        fields = ['id', 'user', 'challenge', 'selfCommitment', 'selfCommitment_reason', 'pushNotification_time', 'hypothesises', 'daily_monitorings', 'weekly_monitorings', 'final_monitoring', 'posts', 'factors']
