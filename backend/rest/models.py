import datetime
from datetime import timedelta

from django.contrib.auth.models import User, AbstractUser
from django.db import models
from django.utils import timezone
from django.utils.datetime_safe import datetime
from django.utils.functional import cached_property
from django.core.validators import MaxValueValidator, MinValueValidator

IMAGE_PATH = "images/challenges"
COLOR_HELPTEXT = "Hex code for color. Format #RRGGBB"

class Account(AbstractUser):
    class Meta:
        db_table = 'auth_user'

    pseudonymous = models.BooleanField(default=False, blank=False, null=False)
    password = models.CharField('password', max_length=128, blank=True)
    data_usage = models.BooleanField(default=False, null=False, help_text="True if the user accepts the data usage and processing rules")

    def __str__(self):
        return self.username


class Friendship(models.Model):
    # related_name has to be specified because auto generation fails when having to foreign keys
    # to the same entity, in this case account
    # for explanation of related_name see:
    # https://stackoverflow.com/questions/2642613/what-is-related-name-used-for-in-django
    sender = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="friendship_sender_set")
    receiver = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="friendship_receiver_set")
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.sender.username + " friend with " + self.receiver.username


class FriendRequest(models.Model):
    sender = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="friend_request_sender_set")
    receiver = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="friend_request_receiver_set")
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.sender.username + " sent friend request to " + self.receiver.username


class Tip(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100)
    description = models.TextField()
    image = models.ImageField(upload_to='images/tips')

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['created']


class TipSchedule(models.Model):
    tip = models.ForeignKey(Tip, on_delete=models.CASCADE)
    scheduled_at = models.DateField()

    def __str__(self):
        return self.scheduled_at

    class Meta:
        ordering = ['scheduled_at']


class Topic(models.Model):
    _img_path = 'images/topics/'

    SUPPORTED_TOPICS = [
        (0, "Forest"),
        (1, "Waste"),
        (2, "Water / Ocean"),
        (3, "Food"),
        (4, "Travelling"),
        (5, "Shopping"),
        (6, "Energy")
    ]

    # The internal id is used to specify the topic
    internal_id = models.IntegerField(unique=True, choices=SUPPORTED_TOPICS)
    topic_name = models.CharField(max_length=25, help_text="Name of Topic")
    topic_description = models.CharField(max_length=256, blank=True,
                                         help_text="A short, wordy description of the topic.")
    image_level1 = models.ImageField(upload_to=_img_path, blank=True)
    image_level2 = models.ImageField(upload_to=_img_path, blank=True)
    image_level3 = models.ImageField(upload_to=_img_path, blank=True)
    image_level4 = models.ImageField(upload_to=_img_path, blank=True)
    image_level5 = models.ImageField(upload_to=_img_path, blank=True)

    def __str__(self):
        return " {} - {}".format(self.SUPPORTED_TOPICS[self.internal_id][1], self.topic_name)


class SelfCommitment(models.Model):
    self_commitment1 = models.CharField(max_length=50, blank=True)
    self_commitment2 = models.CharField(max_length=50, blank=True)
    self_commitment3 = models.CharField(max_length=50, blank=True)
    self_commitment4 = models.CharField(max_length=50, blank=True)
    self_commitment5 = models.CharField(max_length=50, blank=True)


class Challenge(models.Model):
    class Meta:
        ordering = ['title']

    DIFFICULTY_CHOICES = [
        ("EASY", "Easy"),
        ("ADVANCED", "Advanced"),
        ("HARD", "Hard"),
    ]

    PERIODICITY_CHOICES = [
        ("DAILY", "Daily"),
        ("WEEKLY", "Weekly"),
        ("MONTHLY", "Monthly"),
    ]

    CATEGORY_CHOICES = [
        ("MOBILITY", "Mobilität"),
        ("SHOPPING", "Shopping"),
        ("LIFE", "Leben"),
        ("WORK_EDU", "Arbeiten / Ausbildung"),
    ]

    title = models.CharField(max_length=100)
    description = models.TextField()
    icon = models.ImageField(upload_to=IMAGE_PATH, blank=True)
    image = models.ImageField(upload_to=IMAGE_PATH, blank=True)
    duration = models.IntegerField(help_text="Duration of the challenge")
    color = models.CharField(max_length=9, blank=True, default="#006B2A", help_text=COLOR_HELPTEXT)
    difficulty = models.CharField(
        max_length=10,
        choices=DIFFICULTY_CHOICES,
        default=DIFFICULTY_CHOICES[0][0]
    )
    periodicity = models.CharField(
        max_length=10,
        choices=PERIODICITY_CHOICES,
        default=PERIODICITY_CHOICES[0][0]
    )
    topic = models.ForeignKey(Topic, on_delete=models.PROTECT, related_name="topic")
    category = models.CharField(
        max_length=10,
        choices=CATEGORY_CHOICES,
        default=CATEGORY_CHOICES[0][0])
    selfCommitment = models.ForeignKey(SelfCommitment, on_delete=models.CASCADE,
                                       related_name="challenge_selfCommitment")

    def __str__(self):
        return self.title


class UserChallenge(models.Model):
    class Meta:
        ordering = ['title']

    DIFFICULTY_CHOICES = [
        ("EASY", "Easy"),
        ("ADVANCED", "Advanced"),
        ("HARD", "Hard"),
    ]

    PERIODICITY_CHOICES = [
        ("DAILY", "Daily"),
        ("WEEKLY", "Weekly"),
        ("MONTHLY", "Monthly"),
    ]

    CATEGORY_CHOICES = [
        ("MOBILITY", "Mobilität"),
        ("SHOPPING", "Shopping"),
        ("LIFE", "Leben"),
        ("WORK_EDU", "Arbeiten / Ausbildung"),
    ]

    title = models.CharField(max_length=100)
    description = models.TextField()
    icon = models.ImageField(upload_to=IMAGE_PATH, blank=True)
    image = models.ImageField(upload_to=IMAGE_PATH, blank=True)
    duration = models.IntegerField(help_text="Duration of the challenge")
    color = models.CharField(max_length=9, help_text=COLOR_HELPTEXT)
    difficulty = models.CharField(
        max_length=10,
        choices=DIFFICULTY_CHOICES,
        default=DIFFICULTY_CHOICES[0][0]
    )
    periodicity = models.CharField(
        max_length=10,
        choices=PERIODICITY_CHOICES,
        default=PERIODICITY_CHOICES[0][0]
    )
    topic = models.ForeignKey(Topic, on_delete=models.PROTECT, related_name="userChallenge_topic")
    category = models.CharField(
        max_length=10,
        choices=CATEGORY_CHOICES,
        default=CATEGORY_CHOICES[0][0])
    selfCommitment = models.ForeignKey(SelfCommitment, on_delete=models.CASCADE,
                                       related_name="userChallenge_selfCommitment")
    user = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="userChallenge")
    internal_id = models.IntegerField()


class ChallengeParticipation(models.Model):
    class Meta:
        unique_together = (('user', 'challenge'),)

    joined_time = models.DateTimeField(default=timezone.now, help_text="Time when the user joined the challenge")
    user = models.ForeignKey(Account, on_delete=models.CASCADE, help_text="Linked User")
    challenge = models.ForeignKey(UserChallenge, on_delete=models.CASCADE, help_text="Linked user challenge which may differ from the original challenge")
    mark_deleted = models.BooleanField(default=False, help_text="True if the Record is marked to delete")
    shared = models.BooleanField(default=False, help_text="True if the participation is visible to the user's friends")

    @cached_property
    def progress_loggable(self):
        """ Calculates wheter or not progress can logged """
        today = datetime.now().date()
        periodicity = self.challenge.periodicity
        if periodicity == 'DAILY':
            today = today - timedelta(days=1)
        elif periodicity == 'WEEKLY':
            today = today - timedelta(weeks=1)
        elif periodicity == 'MONTHLY':
            today = today - timedelta(weeks=4)
        else:
            pass

        try:
            # load latest progress (could not exists, therefore try except)
            last_progress = self.progress.latest('create_time')
        except ChallengeProgress.DoesNotExist:
            pass

        progress_count = self.progress.count()
        # latest progress must be X days/weeks/months ago AND total count must be lower than total duration
        return progress_count == 0 or (
                last_progress.create_time.date() <= today and progress_count < self.challenge.duration)


class ChallengeProgress(models.Model):
    create_time = models.DateTimeField(default=timezone.now,
                                       help_text="Time when the progress was modified")
    participation = models.ForeignKey(ChallengeParticipation, on_delete=models.CASCADE,
                                      help_text="Linked ChallengeParticipation",
                                      related_name='progress')  # related_name makes progress available on ChallengeParticipation
    mark_deleted = models.BooleanField(default=False, help_text="Returns true if the Record is marked to delete");


class Reward(models.Model):
    TYPE_CHOICES = [
        ("BADGE", "Badge"),
        ("MEDAL", "Medal"),
        ("TROPHY", "Trophy"),
    ]

    type = models.CharField(
        max_length=10,
        choices=TYPE_CHOICES,
        default=TYPE_CHOICES[0]
    )
    color = models.CharField(max_length=9, help_text=COLOR_HELPTEXT)
    points = models.IntegerField()
    condition = models.IntegerField()


class AchievedReward(models.Model):
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    reward = models.ForeignKey(Reward, on_delete=models.CASCADE)
    achieved_time = models.DateTimeField(auto_now_add=True, editable=False,
                                         help_text="Time when the user achieved that reward")

    class Meta:
        unique_together = ('user', 'reward')


class TopicLevel(models.Model):
    name = models.CharField(max_length=30, help_text="Level name, e.g 'Rookie'.")
    limit = models.IntegerField(help_text="The max score to which this level goes, "
                                          "exceeding this score will result in moving on to the next level.")
    icon = models.ImageField(upload_to='images/topic-levels', blank=False)


# Diary model - Diary Feature
class Diary(models.Model):
    user = models.ForeignKey(Account, on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="Linked user", related_name='diary')
    challenge = models.ForeignKey(UserChallenge, on_delete=models.CASCADE, null=True, blank=True, help_text="Linked challenge", related_name='diary')
    selfCommitment = models.CharField(max_length=50)
    selfCommitment_reason = models.TextField(help_text="Reason why a Self-Commitment was chosen")
    pushNotification_time = models.TimeField(help_text="Time for the push Notifications")


# Hypothesis model - Diary Feature
class Hypothesis(models.Model):
    diary = models.ForeignKey(Diary, on_delete=models.CASCADE, related_name='hypothesises')
    startHypothesis = models.BooleanField(default=True)
    endHypothesis = models.BooleanField(default=False)
    difficulty = models.DecimalField(max_digits=3, decimal_places=2, validators=[MaxValueValidator(1), MinValueValidator(0)],
                                     help_text="How difficult is the Self-Commitment? Integer between 0 and 1")
    limitation = models.DecimalField(max_digits=3, decimal_places=2, validators=[MaxValueValidator(1), MinValueValidator(0)],
                                     help_text="How much the Self-Commitment limits me? Integer between 0 and 1")
    environment = models.DecimalField(max_digits=3, decimal_places=2, validators=[MaxValueValidator(1), MinValueValidator(0)],
                                      help_text="How does my environment react to my self-commitment? Integer between 0 and 1")
    contribution = models.DecimalField(max_digits=3, decimal_places=2, validators=[MaxValueValidator(1), MinValueValidator(0)],
                                       help_text="What contribution can this self-commitment make? Integer between 0 and 1")
    burden = models.DecimalField(max_digits=3, decimal_places=2, validators=[MaxValueValidator(1), MinValueValidator(0)],
                                 help_text="How much will this self-commitment burden me? Integer between 0 and 1")


# Daily Monitoring - Diary Feature
class DailyMonitoring(models.Model):
    diary = models.ForeignKey(Diary, on_delete=models.CASCADE, related_name='daily_monitorings')
    wellbeing = models.DecimalField(max_digits=3, decimal_places=2, validators=[MaxValueValidator(1), MinValueValidator(0)],
                                    help_text="How is your wellbeing? Integer between 0 and 1")
    done = models.BooleanField(default=True)


# Weekly Monitoring - Diary Feature
class WeeklyMonitoring(models.Model):
    diary = models.ForeignKey(Diary, on_delete=models.CASCADE, related_name='weekly_monitorings')
    week = models.IntegerField(validators=[MaxValueValidator(4), MinValueValidator(1)],
                               help_text="Week associated with the challenge")
    self_action = models.TextField(help_text="User input for an action for yourself")
    socialEnvironment_action = models.TextField(help_text="User input for an action in the social environment")
    political_action = models.TextField(help_text="User input for an action on a political level")
    producer_action = models.TextField(help_text="User input for an action on a producer level")


# Final Monitoring - Diary Feature
class FinalMonitoring(models.Model):
    diary = models.ForeignKey(Diary, on_delete=models.CASCADE, related_name='final_monitoring')


# Posts for diary - Diary Feature
class Post(models.Model):
    diary = models.ForeignKey(Diary, on_delete=models.CASCADE, related_name='posts')
    text = models.TextField(help_text="User input for the post")
    positive = models.BooleanField(default=True,
                                   help_text="True if the post is positive, False if the post is negative")
    week = models.IntegerField(validators=[MaxValueValidator(4), MinValueValidator(1)],
                               help_text="Week associated with the challenge")


# Factors for diary - Diary Feature
class Factor(models.Model):
    diary = models.ForeignKey(Diary, on_delete=models.CASCADE, related_name='factors')
    text = models.CharField(max_length=100, help_text="User input for the factor")
    positive = models.BooleanField(default=True,
                                   help_text="True if the factor is positive, False if the factor is negative")
    finalFactor = models.BooleanField(default=False, help_text="True if the factor is a final factor")
