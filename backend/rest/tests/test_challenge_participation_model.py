from datetime import timedelta

from django.test import TestCase
from django.utils import timezone

from ..models import Challenge, Topic, Account, ChallengeParticipation, ChallengeProgress, UserChallenge, SelfCommitment


class TestChallengeParticipationModel(TestCase):
    """Tests the correct working of the computed property of challenge participations """

    def setUp(self):
        self.test_user = Account(username='testuser232525')
        self.test_user.save()
        topic, created = Topic.objects.get_or_create(internal_id=1, topic_name='Abfall')
        selfCommitment, created = SelfCommitment.objects.get_or_create(self_commitment1="", self_commitment2="", self_commitment3="", self_commitment4="", self_commitment5="")
        self.challenge = UserChallenge(title="A very nice challenge", difficulty="EASY", duration=10,
                                   description="Lorem ipsum dolorus", topic=topic,
                                   periodicity="WEEKLY", selfCommitment=selfCommitment, user=self.test_user, internal_id=1)
        self.challenge.save()

    def testProgressLoggable(self):
        participation = ChallengeParticipation(user=self.test_user, challenge=self.challenge)
        participation.save()
        ChallengeProgress(participation=participation, create_time=timezone.now() - timedelta(days=7)).save()

        self.assertEquals(participation.progress_loggable, True)

    def testProgressNOTLoggable(self):
        participation = ChallengeParticipation(user=self.test_user, challenge=self.challenge)
        participation.save()
        ChallengeProgress(participation=participation, create_time=timezone.now() - timedelta(days=5)).save()

        self.assertEquals(participation.progress_loggable, False)

    def testProgressNotLoggableDueToDurationLimit(self):
        topic, created = Topic.objects.get_or_create(internal_id=1, topic_name='Abfall')
        selfCommitment, created = SelfCommitment.objects.get_or_create(self_commitment1="", self_commitment2="", self_commitment3="", self_commitment4="", self_commitment5="")
        challenge2 = UserChallenge(title="A challenge with just one duration", difficulty="EASY", duration=1,
                                   description="Lorem ipsum dolorus", topic=topic,
                                   periodicity="DAILY", selfCommitment=selfCommitment, user=self.test_user, internal_id=1)
        challenge2.save()

        participation = ChallengeParticipation(user=self.test_user, challenge=challenge2)
        participation.save()
        ChallengeProgress(participation=participation, create_time=timezone.now() - timedelta(days=5)).save()

        self.assertEquals(participation.progress_loggable, False)