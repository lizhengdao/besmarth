from django.urls import reverse
from rest_framework import status

from .bne_base import BneBaseTest
from ..models import Account, Challenge, Friendship, ChallengeParticipation, ChallengeProgress, Topic, UserChallenge, SelfCommitment


class TestAccountFriendChallengesView(BneBaseTest):

    def setUp(self):
        super().setUp()
        self.lukas = Account(username="lukas95")
        self.claudia = Account(username="claudia99")
        self.enemy = Account(username="enemy97")
        self.supply_objects(self.enemy, self.lukas, self.claudia)

        self.fs_me_lukas = Friendship(sender=self.test_user, receiver=self.lukas)
        self.fs_claudia_me = Friendship(sender=self.claudia, receiver=self.test_user)

        self.supply_objects(self.fs_me_lukas, self.fs_claudia_me)

        topic, created = Topic.objects.get_or_create(internal_id=1, topic_name='Waste topic')
        selfCommitment, created = SelfCommitment.objects.get_or_create(self_commitment1="", self_commitment2="", self_commitment3="", self_commitment4="", self_commitment5="")

        challenge1 = UserChallenge(title="A very nice challenge", difficulty="EASY", duration=10,
                               description="Lorem ipsum dolorus", topic=topic,
                               periodicity="WEEKLY", selfCommitment=selfCommitment, user=self.lukas, internal_id=1)
        challenge2 = UserChallenge(title="Yet another nice challenge", difficulty="HARD", description="Lorem ipsum",
                               duration=10, topic=topic,
                               periodicity="DAILY", selfCommitment=selfCommitment, user=self.lukas, internal_id=1)
        self.supply_objects(challenge1, challenge2)

        lukas_part_challenge_1 = ChallengeParticipation(user=self.lukas, challenge=challenge1, shared=True,
                                                        mark_deleted=False)
        lukas_part_challenge_2 = ChallengeParticipation(user=self.lukas, challenge=challenge2, shared=False,
                                                        mark_deleted=False)
        claudia_part_challenge_1 = ChallengeParticipation(user=self.claudia, challenge=challenge1, shared=False,
                                                          mark_deleted=False)
        claudia_part_challenge_2 = ChallengeParticipation(user=self.claudia, challenge=challenge2, shared=True,
                                                          mark_deleted=False)

        self.supply_objects(lukas_part_challenge_1, lukas_part_challenge_2, claudia_part_challenge_1,
                            claudia_part_challenge_2)

        ChallengeProgress(participation=lukas_part_challenge_1, mark_deleted=False).save()
        ChallengeProgress(participation=lukas_part_challenge_2, mark_deleted=False).save()
        ChallengeProgress(participation=claudia_part_challenge_1, mark_deleted=False).save()
        ChallengeProgress(participation=claudia_part_challenge_2, mark_deleted=False).save()

    def testForbiddenAccessIfNoFriend(self):
        friend_challenges_url = reverse('account-friend-challenges', args=[self.enemy.id])
        response = self.get_response(friend_challenges_url, exp_status_code=status.HTTP_403_FORBIDDEN)

    def testOnlySharedChallengesOfMyFriend(self):
        friend_challenges_url = reverse('account-friend-challenges', args=[self.lukas.id])
        response = self.get_response(friend_challenges_url)
        self.assertEquals(len(response.data), 1)
        self.assertEquals(response.data[0]['shared'], True)
        self.assertEquals(response.data[0]['challenge']['title'], "A very nice challenge")
