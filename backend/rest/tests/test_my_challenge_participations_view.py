from datetime import timedelta

from django.urls import reverse
from django.utils import timezone

from .bne_base import BneBaseTest
from ..models import Account, Challenge, ChallengeParticipation, ChallengeProgress, Topic, UserChallenge, SelfCommitment


class TestMyChallengeParticipationsView(BneBaseTest):

    def setUp(self):
        super().setUp()
        self.lukas = Account(username="lukas95")

        self.supply_objects(self.lukas)

        topic, created = Topic.objects.get_or_create(internal_id=1, topic_name='Abfall')
        selfCommitment, created = SelfCommitment.objects.get_or_create(self_commitment1="", self_commitment2="", self_commitment3="", self_commitment4="", self_commitment5="")

        challenge1 = UserChallenge(title="A very nice challenge", difficulty="EASY", duration=10,
                               description="Lorem ipsum dolorus", topic=topic,
                               periodicity="WEEKLY", selfCommitment=selfCommitment, user=self.lukas, internal_id=1)
        challenge2 = UserChallenge(title="Yet another nice challenge", difficulty="HARD", description="Lorem ipsum",
                               duration=10, topic=topic,
                               periodicity="DAILY", selfCommitment=selfCommitment, user=self.lukas, internal_id=1)
        self.supply_objects(challenge1, challenge2)

        lukas_part_challenge_1 = ChallengeParticipation(user=self.lukas, challenge=challenge1, shared=True,
                                                        mark_deleted=False)
        lukas_part_challenge_2 = ChallengeParticipation(user=self.lukas, challenge=challenge2, shared=False,
                                                        mark_deleted=False)
        my_challenge_1 = ChallengeParticipation(user=self.test_user, challenge=challenge1, shared=False,
                                                mark_deleted=False)
        my_challenge_2 = ChallengeParticipation(user=self.test_user, challenge=challenge2, shared=True,
                                                mark_deleted=False)

        self.supply_objects(lukas_part_challenge_1, lukas_part_challenge_2, my_challenge_1,
                            my_challenge_2)

        ChallengeProgress(participation=lukas_part_challenge_1, mark_deleted=False).save()
        ChallengeProgress(participation=lukas_part_challenge_2, mark_deleted=False).save()
        ChallengeProgress(participation=my_challenge_1, mark_deleted=False,
                          create_time=timezone.now() - timedelta(days=10)).save()
        ChallengeProgress(participation=my_challenge_2, mark_deleted=False,
                          create_time=timezone.now() + timedelta(days=-1)).save()
        ChallengeProgress(participation=my_challenge_2, mark_deleted=False).save()

    def testMyChallenges(self):
        my_challenges_url = reverse('my-challenges')
        response = self.get_response(my_challenges_url)

        self.assertEquals(len(response.data), 2)
        self.assertEquals(len(response.data[0]['progress']), 1)
        self.assertEquals(response.data[0]['challenge']['title'], 'A very nice challenge')
        self.assertEquals(response.data[0]['challenge']['topic'], 'Abfall')
        self.assertEquals(response.data[0]['progress_loggable'], True)

        self.assertEquals(len(response.data[1]['progress']), 2)
        self.assertEquals(response.data[1]['progress_loggable'], False)
        self.assertEquals(response.data[1]['challenge']['title'], 'Yet another nice challenge')
        self.assertEquals(response.data[1]['challenge']['topic'], 'Abfall')
