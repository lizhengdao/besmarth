from django.urls import reverse
from rest_framework import status

from .bne_base import BneBaseTest
from ..models import Account, Friendship, Topic, Challenge, ChallengeProgress, ChallengeParticipation


class TestAccountsView(BneBaseTest):

    def setUp(self):
        super().setUp()

    def assert_contains_username(self, accounts, username, contained=True):
        contains = False
        for account in accounts:
            if (account['username'] == username):
                contains = True
        self.assertTrue(contains == contained)

    def test_read_accounts_empty_query(self):
        user1 = Account(username="user1")
        user2 = Account(username="user2")
        self.supply_objects(user1, user2)

        accounts_url = reverse('accounts')
        response = self.get_response(accounts_url)
        accounts = response.data

        self.assertTrue(len(accounts) >= 3)
        self.assert_contains_username(accounts, user1.username)
        self.assert_contains_username(accounts, user2.username)
        self.assert_contains_username(accounts, self.test_user.username)

    def test_read_accounts_query_no_results(self):
        user1 = Account(username="user1")
        user2 = Account(username="user2")
        self.supply_objects(user1, user2)

        accounts_url = reverse('accounts')
        accounts_url += "?query=thisuserdoesnotexist"
        response = self.get_response(accounts_url)
        accounts = response.data

        self.assertTrue(len(accounts) == 0)

    def test_read_accounts_query_with_results(self):
        user1 = Account(username="unique_testuser1")
        user2 = Account(username="unique_testuser2")
        self.supply_objects(user1, user2)

        accounts_url = reverse('accounts')
        accounts_url += "?query=unIQue_teStuser"
        response = self.get_response(accounts_url)
        accounts = response.data

        self.assertTrue(len(accounts) == 2)
        self.assert_contains_username(accounts, user1.username)
        self.assert_contains_username(accounts, user2.username)
        self.assert_contains_username(accounts, self.test_user.username, False)

