from collections import OrderedDict
from datetime import timedelta

from django.urls import reverse
from django.utils import timezone

from .bne_base import BneBaseTest
from ..models import Account, Challenge, ChallengeParticipation, ChallengeProgress, Topic, Friendship, UserChallenge, SelfCommitment


class TestAccountExportView(BneBaseTest):

    def setUp(self):
        super().setUp()

        self.test_user.email = 'atestmail@web.de'
        self.test_user.last_login = timezone.now()
        self.test_user.save()

        self.lukas = Account(username="lukas95", first_name='Lukas')
        self.claudia = Account(username="claudia978", first_name='Claudia')

        self.supply_objects(self.lukas, self.claudia)

        topic, created = Topic.objects.get_or_create(internal_id=1, topic_name='Abfall')
        selfCommitment, created = SelfCommitment.objects.get_or_create(self_commitment1="", self_commitment2="", self_commitment3="", self_commitment4="", self_commitment5="")

        challenge1 = UserChallenge(title="A very ni12ce challenge", difficulty="EASY", duration=10,
                               description="Lorem ipsum dolorus", topic=topic,
                               periodicity="WEEKLY", selfCommitment=selfCommitment, user=self.lukas, internal_id=1)
        challenge2 = UserChallenge(title="Yet another nice challenge", difficulty="HARD", description="Lorem ipsum",
                               duration=10, topic=topic,
                               periodicity="DAILY", selfCommitment=selfCommitment, user=self.lukas, internal_id=1)
        self.supply_objects(challenge1, challenge2)

        my_challenge_1 = ChallengeParticipation(user=self.test_user, challenge=challenge1, shared=False,
                                                mark_deleted=False)
        my_challenge_2 = ChallengeParticipation(user=self.test_user, challenge=challenge2, shared=True,
                                                mark_deleted=False)

        self.supply_objects(my_challenge_1,
                            my_challenge_2)

        friendship_lukas = Friendship(sender=self.test_user, receiver=self.lukas)
        friendship_claudia = Friendship(sender=self.claudia, receiver=self.test_user)

        self.supply_objects(friendship_lukas, friendship_claudia)

        ChallengeProgress(participation=my_challenge_1, mark_deleted=False,
                          create_time=timezone.now() - timedelta(days=11)).save()
        ChallengeProgress(participation=my_challenge_2, mark_deleted=False,
                          create_time=timezone.now() + timedelta(days=-1)).save()
        ChallengeProgress(participation=my_challenge_2, mark_deleted=False).save()

    def test_export(self):
        export_url = reverse('account-data')

        response = self.get_response(export_url)

        self.assertEquals(response.data['account']['id'], 1)
        self.assertEquals(response.data['account']['first_name'], 'Hugo')
        self.assertEquals(response.data['account']['username'], 'TestUser')
        self.assertEquals(response.data['account']['last_name'], 'Habicht')
        self.assertEquals(response.data['account']['email'], 'atestmail@web.de')
        self.assertEquals(response.data['account']['pseudonymous'], False)
        self.assertEquals(response.data['account']['is_active'], True)
        self.assertIsNotNone(response.data['account']['date_joined'])
        self.assertIsNotNone(response.data['account']['last_login'])

        self.assertDictEqual(response.data['friendships'][0]['sender'],
                             OrderedDict([("id", 3), ("username", 'claudia978'), ("first_name", 'Claudia')]))
        self.assertDictEqual(response.data['friendships'][0]['receiver'],
                             OrderedDict([("id", 1), ("username", 'TestUser'), ("first_name", 'Hugo')]))
        self.assertIsNotNone(response.data['friendships'][0]['date'])

        self.assertDictEqual(response.data['friendships'][1]['sender'],
                             OrderedDict([("id", 1), ("username", 'TestUser'), ("first_name", 'Hugo')]))
        self.assertDictEqual(response.data['friendships'][1]['receiver'],
                             OrderedDict([("id", 2), ("username", 'lukas95'), ("first_name", 'Lukas')]))
        self.assertIsNotNone(response.data['friendships'][1]['date'])

        self.assertEquals(response.data['participations'][0]['challenge']['id'], 1);
        self.assertEquals(response.data['participations'][0]['shared'], False);
        self.assertEquals(response.data['participations'][0]['mark_deleted'], False);
        self.assertEquals(response.data['participations'][0]['progress_loggable'], True);
        self.assertEquals(response.data['participations'][0]['progress'][0]['id'], 1);
        self.assertIsNotNone(response.data['participations'][0]['progress'][0]['create_time']);
        self.assertIsNotNone(response.data['participations'][0]['progress'][0]['mark_deleted'],
                             self.assertEquals(response.data['participations'][0]['progress'][0]['mark_deleted'],
                                               False));

        self.assertEquals(response.data['participations'][1]['challenge']['id'], 2);
        self.assertEquals(response.data['participations'][1]['shared'], True);
        self.assertEquals(response.data['participations'][1]['mark_deleted'], False);
        self.assertEquals(response.data['participations'][1]['progress_loggable'], False);
        self.assertEquals(response.data['participations'][1]['progress'][0]['id'], 2);
        self.assertIsNotNone(response.data['participations'][1]['progress'][0]['create_time']);
        self.assertIsNotNone(response.data['participations'][1]['progress'][0]['mark_deleted'],
                             self.assertEquals(response.data['participations'][1]['progress'][0]['mark_deleted'],
                                               False));
