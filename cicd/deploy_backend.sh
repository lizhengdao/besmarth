#!/bin/bash

# causes the script to abort for any command that fails
set -e

CI_BUILD_TOKEN="{CI_BUILD_TOKEN}"
BACKEND_LATEST="{BACKEND_LATEST}"
DJANGO_ENV="{DJANGO_ENV}"

docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
docker pull $BACKEND_LATEST
docker container stop besmarth_backend && docker container rm besmarth_backend || true

docker run -d \
--volume besmarth_backend:/var/lib/besmarth \
--network besmarth \
--name besmarth_backend \
--env ENVIRONMENT=$DJANGO_ENV \
--env "VIRTUAL_HOST=besmarth.ch,www.besmarth.ch" \
--env "LETSENCRYPT_HOST=besmarth.ch,www.besmarth.ch" \
--expose 80 \
$BACKEND_LATEST