import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, ScrollView, View, SectionList} from 'react-native';
import {Text, Paragraph, Surface, Button, FAB, TextInput, ProgressBar, Divider} from "react-native-paper";
import Navigation from "../services/Navigation";
import {Ionicons} from "@expo/vector-icons";
import {Colors} from "../styles/index";

class WeeklyFactorsOverviewScreen extends React.Component {

    data = {};

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            summary: "",
            summaryList: [
                {
                    title: "Erleichternd",
                    data: []
                },
                {
                    title: "Erschwerend",
                    data: []
                }
            ],
            minimalFactorList: []
        };
        this.data = this.props?.route?.params.data;
    }

    navigateToWeeklyActionsButton() {
        this.data.factors = this.state.minimalFactorList;
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Weiter"
            accessibilityLabel="Weiter"
            onPress={() => Navigation.navigate("DiaryTab", "WeeklyActions", {data: this.data})}/>);
    }

    getPostList() {
        const postList = [
            {
                title: "Erleichternd",
                data: []
            },
            {
                title: "Erschwerend",
                data: []
            }
        ];
        let postsData = this.data.diary.posts;
        postsData = postsData.filter(post => post.week === this.data.week);
        postsData.forEach(post => {
            if(post.positive) {
                let postForList = {
                    text: "👍: " + post.text
                };
                postList[0].data.push(postForList);
            } else {
                let postForList = {
                    text: "👎: " + post.text
                };
                postList[1].data.push(postForList);
            }
        })
        return postList;
    }

    listPositiveAndNegativePosts() {
        return (
            <ScrollView>
                <Surface style={styles.card}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getPostList()}
                        renderItem={({item}) => <Text style={styles.item}>{item.text}</Text>}
                        keyExtractor={(item, index) => index}
                        renderSectionHeader={({section: {title}}) => (
                            <Text style={styles.sectionHeader}>{title}</Text>
                        )}
                    />
                </Surface>
            </ScrollView>
        )
    }

    onChangeField(fieldName, newValue) {
        this.setState({
            ...this.state,
            [fieldName]: newValue
        });
    }

    summaryList() {
        if(this.state.minimalFactorList.length !== 0) {
            return (
            <ScrollView>
                <Surface style={styles.card}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.state.summaryList}
                        renderItem={({item}) => <Text style={styles.item}>{item.text}</Text>}
                        keyExtractor={(item, index) => index}
                        renderSectionHeader={({section: {title}}) => (
                            <Text style={styles.sectionHeader}>{title}</Text>
                        )}
                    />
                </Surface>
            </ScrollView>
        )
        }
    }

    addPositiveSummary() {
        //Add minimalFactor for backend
        let minimalFactor = {};
        minimalFactor.text = this.state.summary;
        minimalFactor.positive = true;
        minimalFactor.finalFactor = false;
        this.state.minimalFactorList.push(minimalFactor);
        //Add summary for frontend to summaryList
        let summary = {};
        summary.text = "👍: " + this.state.summary;
        let summaryList = this.state.summaryList;
        summaryList[0].data.push(summary);
        this.setState({...this.state, summary: "", postList: summaryList});
    }

    addNegativeSummary() {
        //Add minimalFactor for backend
        let minimalFactor = {};
        minimalFactor.text = this.state.summary;
        minimalFactor.positive = true;
        minimalFactor.finalFactor = false;
        this.state.minimalFactorList.push(minimalFactor);
        //Add summary for frontend to summaryList
        let summary = {};
        summary.text = "👎: " + this.state.summary;
        let summaryList = this.state.summaryList;
        summaryList[1].data.push(summary);
        this.setState({...this.state, summary: "", postList: summaryList});
    }

    writeOwnSummaryOfWeek() {
        return (
            <View style={styles.row}>
                <TextInput
                    style={styles.formControl}
                    label="Benenne diese Faktoren..."
                    value={this.state.summary}
                    accessibilityLabel="Benenne diese Faktoren..."
                    onChangeText={val => this.onChangeField("summary", val)}
                />
                <Button disabled={this.state.summary === ""} onPress={() => this.addPositiveSummary()}><Ionicons style={styles.rightIcon} name={"ios-thumbs-up"}
                                                                      size={25}/></Button>
                <Button disabled={this.state.summary === ""} onPress={() => this.addNegativeSummary()}><Ionicons style={styles.rightIcon} name={"ios-thumbs-down"}
                                                                      size={25}/></Button>
            </View>
        );
    }

    render() {
        const selfCommitment = this.data.diary.selfCommitment;
        const summaryDesc = "Was zeigen Deine Daten, wie ist es Dir diese Woche mit dem Self-Commitment ergangen? Und was zeigen deine Posts, welche Faktoren haben die Einlösung Deines Self-Commitments vereinfacht, welche haben es erschwert?"

        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>2 von 3</Text>
                <ProgressBar progress={0.66} style={{marginBottom: 20}}/>
                <Paragraph>Posts von "{selfCommitment}":</Paragraph>
                {this.listPositiveAndNegativePosts()}
                <Paragraph/>
                <Divider/>
                <Paragraph/>
                <Paragraph>{summaryDesc}</Paragraph>
                {this.summaryList()}
                {this.writeOwnSummaryOfWeek()}
                {this.navigateToWeeklyActionsButton()}
                <Paragraph/>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center"
    },
    sectionHeader: {
        fontSize: 15,
        fontWeight: 'bold',
    },
    item: {
        fontSize: 14,
    },
    formControl: {
        marginVertical: 10,
        width: 250
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 5,
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(WeeklyFactorsOverviewScreen);