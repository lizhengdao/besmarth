import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, ScrollView} from 'react-native';
import {Text, Paragraph, TextInput, FAB, ProgressBar, Divider} from "react-native-paper";
import Navigation from "../services/Navigation";
import {Colors} from "../styles/index";
import {showToast} from "../services/Toast";
import {createFactor, createWeeklyMonitoring} from "../services/DiaryService";

class WeeklyActionsScreen extends React.Component {

    data = {};

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            ownActions: "",
            socialActions: "",
            politicalActions: "",
            productManufacturerActions: "",
        };
        this.data = this.props?.route?.params.data;
    }

    navigateToExperienceOverviewButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Abschliessen"
            accessibilityLabel="Abschliessen"
            onPress={() => this.weeklyFinished()}/>);
    }

    async weeklyFinished() {
        let factors = this.data.factors;
        let diaryId = this.data.diary.id;
        await factors.forEach(factor => {
            createFactor(diaryId, factor);
        })
        await createWeeklyMonitoring(diaryId, this.getWeeklyMonitoringData());
        Navigation.navigate("DiaryTab", "ExperienceOverview")
        this.props.showToast("Wöchentlicher Statusbericht abgeschlossen!")
        if (this.getWeeklyMonitoringData().week === 4) {
            // TODO: Send notification that final report is now available!
        }
    }

    getWeeklyMonitoringData() {
        let weeklyMonitoring = {
            week: this.data.week,
            self_action: this.state.ownActions,
            socialEnvironment_action: this.state.socialActions,
            political_action: this.state.politicalActions,
            producer_action: this.state.productManufacturerActions
        };
        return weeklyMonitoring;
    }

    onChangeField(fieldName, newValue) {
        this.setState({
            ...this.state,
            [fieldName]: newValue
        });
    }

    actionsWithTextField() {
        return (
            <View>
                <TextInput
                    multiline={true}
                    label="...die man selbst umsetzen kann"
                    value={this.state.ownActions}
                    accessibilityLabel="Massnahmen die man selbst umsetzen kann"
                    onChangeText={val => this.onChangeField("ownActions", val)}
                />
                <Paragraph/>
                <TextInput
                    multiline={true}
                    label="...für eine Veränderung im sozialen Umfeld"
                    value={this.state.socialActions}
                    accessibilityLabel="Massnahmen für Veränderung im sozialen Umfeld"
                    onChangeText={val => this.onChangeField("socialActions", val)}
                />
                <Paragraph/>
                <TextInput
                    multiline={true}
                    label="...auf politischer Ebene"
                    value={this.state.politicalActions}
                    accessibilityLabel="Massnahmen auf politischer Ebene"
                    onChangeText={val => this.onChangeField("politicalActions", val)}
                />
                <Paragraph/>
                <TextInput
                    multiline={true}
                    label="...auf der Ebene von Produktherstellern"
                    value={this.state.productManufacturerActions}
                    accessibilityLabel="Massnahmen auf der Ebene von Produktherstellern"
                    onChangeText={val => this.onChangeField("productManufacturerActions", val)}
                />
            </View>
        )
    }

    render() {

        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>3 von 3</Text>
                <ProgressBar progress={1.0} style={{marginBottom: 20}}/>
                <Paragraph>Hast Du Vorschläge/Ideen für Massnahmen, die Dich und/oder andere bei diesem Self-Commitment
                    unterstützen würden?</Paragraph>
                <Paragraph/>
                <Divider/>
                <Paragraph>Massnahmen:</Paragraph>
                {this.actionsWithTextField()}
                <Paragraph/>
                {this.navigateToExperienceOverviewButton()}
                <Paragraph/>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    buttonGrey: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.lightGray,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({
    showToast
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(WeeklyActionsScreen);