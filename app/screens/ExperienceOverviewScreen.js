import React from "react";
import {FlatList, StyleSheet, View} from "react-native";
import {bindActionCreators} from "redux";
import {Button, Headline, Surface, Text} from "react-native-paper";
import {connect} from "react-redux";
import Navigation from "../services/Navigation";
import {fetchDiaries} from "../services/DiaryService";

class ExperienceOverviewScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true
        };
    }

    componentDidMount() {
        this.subscription = this.props.navigation.addListener("focus", this.reload.bind(this));
        this.reload().then();
    }

    componentWillUnmount() {
        this.subscription();
    }

    async reload() {
        this.setState({loading: true});
        try {
            let diariesData = await fetchDiaries();
            diariesData = diariesData.filter(diary => diary.user.id === this.props.accountId)
            this.setState({
                loading: false,
                diaryParticipations: diariesData
            });
        } catch (e) {
            console.log(e);
            this.setState({
                error: JSON.stringify(e),
                loading: false
            });
        }
    }

    render() {
        if (this.state.error) {
            return <View style={styles.container}><Text>{this.state.error}</Text></View>;
        }

        const emptyContent = <Headline style={styles.emptyListWrapper}>Du hast leider noch kein Tagebuch offen!</Headline>;

        return (
            <View style={styles.container}>
                <FlatList
                    testID="my-diaries-list"
                    style={styles.diariesList}
                    data={this.state.diaryParticipations}
                    onRefresh={this.reload.bind(this)}
                    refreshing={this.state.loading}
                    keyExtractor={(item) => item.id + ""}
                    ListEmptyComponent={() => emptyContent}
                    renderItem={({item}) =>
                        <DiaryItem
                        participation={item}
                        onLogProgress={this.onLogProgress.bind(this)}
                        />
                    }
                />
            </View>);
    }

    async onLogProgress(participation) {
        /*const updatedParticipation = await createChallengeProgress(participation.challenge);
        this.reload().then();

        if (updatedParticipation.progress.length === updatedParticipation.challenge.duration) {
            this.props.showToast("Gratuliere! Du hast die Challenge " + updatedParticipation.challenge.title + " erfolgreich abgeschlossen");
        }*/
    }
}

function DiaryItem({participation}) {
    return (
        <Surface style={styles.swipeCard}>
            <View style={styles.cardTitle}>
                <View style={{flex: 1}}>
                    <Headline>{participation.challenge.title}</Headline>
                </View>
                <View style={{flex: 0}}>
                    <Text style={styles.category}>{participation.challenge.topic}</Text>
                </View>
            </View>
            <Button style={styles.button} mode="contained" onPress={() => Navigation.navigate("DiaryTab", "DailyObservation", {diary: participation})}>Aktueller Tag</Button>
            <Button disabled={false} style={styles.button} mode="contained" onPress={() => Navigation.push("DiaryTab", "WeeklyExperienceReport", {diary: participation, week: 1})}>Woche 1</Button>
            <Button disabled={true} style={styles.button} mode="contained">Woche 2</Button>
            <Button disabled={true} style={styles.button} mode="contained">Woche 3</Button>
            <Button disabled={true} style={styles.button} mode="contained">Woche 4</Button>
            <Button style={styles.button} mode="contained" onPress={() => Navigation.navigate("DiaryTab", "FinalReportIntro", {diary: participation})}>Schlussbericht</Button>
            <View style={styles.participationContent}>
                <View style={{flex: 1}}>
                    <Text>{participation.selfCommitment}</Text>
                </View>
                {/* TODO: https://vix.digital/blog/technology/react-native-scroll-to/ */}
                <View style={styles.logProgressView}><Button style={styles.button} onPress={() => alert("Nächstes Tagebuch")}>Nächstes</Button></View>
            </View>
        </Surface>);
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
    },
    swipeCard: {
        minHeight: 550,
        borderRadius: 50,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 10,
        margin: 20,
        padding: 30,
        marginHorizontal: 30,
    },
    diariesList: {
        flex: 1,
        width: "100%"
    },
    card: {
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 10,
    },
    cardTitle: {
        display: "flex",
        flexDirection: "row",
        alignItems: "flex-start",
        justifyContent: "flex-end"
    },
    category: {
        backgroundColor: "#f1c40f",
        borderRadius: 10,
        color: "black",
        paddingVertical: 3,
        paddingHorizontal: 10,
    },
    logProgressView: {
        marginLeft: 5
    },
    button: {
        marginVertical: 8,
        minWidth: "50%"
    },
    participationContent: {
        flex: 1,
        display: "flex",
        flexDirection: "row",
        alignItems: "flex-end",
        marginTop: 10
    },
    emptyListWrapper: {
        marginTop: "30%",
        marginHorizontal: 10,
        textAlign: "center"
    }
});

const mapStateToProps = state => {
    let accountId;
    if(state.account.account.id === undefined) {
        accountId = state.account.account.user_id;
    } else {
        accountId = state.account.account.id;
    }
    return {accountId};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ExperienceOverviewScreen);
