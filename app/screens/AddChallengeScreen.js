import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, Text, View, Picker, ScrollView} from 'react-native';
import {TextInput, FAB} from 'react-native-paper';
import {Colors} from  "../styles/index";
import NumericInput from "react-native-numeric-input";
import {createChallenge} from "../services/Challenges";
import Navigation from "../services/Navigation";

class AddChallengeScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            title: null,
            description: null,
            duration: 30,
            difficulty: "EASY",
            periodicity: "DAILY",
            topic: this.props.currentTopicName,
            category: "MOBILITY",
            self_commitment1: "",
            self_commitment2: "",
            self_commitment3: "",
            self_commitment4: "",
            self_commitment5: "",
            disabled: true,
            buttonColor: Colors.lightGray
        };
    }

    async onAddChallenge() {
        const challenge = {
            title: this.state.title,
            description: this.state.description,
            duration: this.state.duration,
            color: "#006B2A",
            difficulty: this.state.difficulty,
            periodicity: this.state.periodicity,
            topic: this.state.topic,
            category: this.state.category,
            selfCommitment: {
                self_commitment1: this.state.self_commitment1,
                self_commitment2: this.state.self_commitment2,
                self_commitment3: this.state.self_commitment3,
                self_commitment4: this.state.self_commitment4,
                self_commitment5: this.state.self_commitment5
            }
        };
        await createChallenge(challenge);
        Navigation.goBack();
    }

    render() {
        let getTopicItems = (
        Object.entries(this.props.topics.topics).map(([key, value]) => {
            return (
                <Picker.Item label={value.topic_name} value={value.topic_name}/>
            )
        })
    )

        return (
            <ScrollView style={styles.container}>
                <TextInput
                    style={styles.formControl}
                    label="Titel"
                    onChangeText={value => {
                        this.setState({...this.state, title: value});
                    }}
                    maxLength={40}
                />
                <TextInput
                    style={styles.formControl}
                    label="Beschreibung"
                    onChangeText={value => {
                        this.setState({...this.state, description: value, disabled: false, buttonColor: Colors.green});
                    }}
                    maxLength={255}
                    multiline={true}
                />
                <View style={styles.pickerContainer}>
                    <View style={styles.pickerView}>
                        <Text>Dauer</Text>
                        <NumericInput
                            value={this.state.duration}
                            onChange={value => this.setState({...this.state, duration: value})}
                            minValue={0}
                            maxValue={30}
                            valueType="integer"
                            rounded={true}
                            editable={false}
                            rightButtonBackgroundColor={Colors.green}
                            leftButtonBackgroundColor={Colors.green}
                            iconStyle={styles.numericinputbutton}
                        />
                    </View>
                    <View style={styles.pickerView}>
                        <Text>Häufigkeit</Text>
                        <Picker selectedValue={this.state.periodicity} onValueChange={value => this.setState({...this.state, periodicity: value})} style={styles.picker}>
                            <Picker.Item label="Täglich" value="DAILY"/>
                            <Picker.Item label="Wöchentlich" value="WEEKLY"/>
                            <Picker.Item label="Monatlich" value="MONTHLY"/>
                        </Picker>
                    </View>
                </View>
                <View style={styles.pickerContainer}>
                    <View style={styles.pickerView}>
                        <Text>Topic</Text>
                        <Picker selectedValue={this.state.topic} onValueChange={value => this.setState({...this.state, topic: value})} style={styles.picker}>
                            {getTopicItems}
                        </Picker>
                    </View>
                    <View style={styles.pickerView}>
                        <Text>Kategorie</Text>
                        <Picker selectedValue={this.state.category} onValueChange={value => this.setState({...this.state, category: value})} style={styles.picker}>
                            <Picker.Item label="Mobilität" value="MOBILITY"/>
                            <Picker.Item label="Shopping" value="SHOPPING"/>
                            <Picker.Item label="Leben" value="LIFE"/>
                            <Picker.Item label="Arbeit/Ausbildung" value="WORK_EDU"/>
                        </Picker>
                    </View>
                </View>
                <TextInput
                    style={styles.formControl}
                    label="Self Commitment 1"
                    onChangeText={value => this.setState({...this.state, self_commitment1: value})}
                    maxLength={60}
                />
                <TextInput
                    style={styles.formControl}
                    label="Self Commitment 2"
                    onChangeText={value => this.setState({...this.state, self_commitment2: value})}
                    maxLength={60}
                />
                <TextInput
                    style={styles.formControl}
                    label="Self Commitment 3"
                    onChangeText={value => this.setState({...this.state, self_commitment3: value})}
                    maxLength={60}
                />
                <TextInput
                    style={styles.formControl}
                    label="Self Commitment 4"
                    onChangeText={value => this.setState({...this.state, self_commitment4: value})}
                    maxLength={60}
                />
                <TextInput
                    style={styles.formControl}
                    label="Self Commitment 5"
                    onChangeText={value => this.setState({...this.state, self_commitment5: value})}
                    maxLength={60}
                />
                <FAB
                    style={{backgroundColor: this.state.buttonColor, marginVertical: 10, marginHorizontal: 30}}
                    label="Challenge erstellen"
                    mode="contained"
                    disabled={this.state.disabled}
                    onPress={this.onAddChallenge.bind(this)}/>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    formControl: {
        marginHorizontal: 30,
        marginVertical: 10
    },
    numericinputbutton: {
        color: Colors.white
    },
    pickerView: {
        marginHorizontal: 10
    },
    pickerContainer: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20,
    },
    picker: {
        marginHorizontal: 10,
        marginVertical: 10,
        width: 150
    }
});

const mapStateToProps = state => {
    const topics = state.topics
    const currentTopicName = state.topics.currentTopicName
    return {topics, currentTopicName};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddChallengeScreen);