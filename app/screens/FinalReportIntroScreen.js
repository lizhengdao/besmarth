import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, ScrollView} from 'react-native';
import {Text, Paragraph, FAB, ProgressBar} from "react-native-paper";
import Navigation from "../services/Navigation";
import {Colors} from "../styles/index";

class FinalReportIntroScreen extends React.Component {

    constructor(props) {
        super(props);
    }

    navigateToFinalReportButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Weiter"
            accessibilityLabel="Weiter"
            onPress={() => Navigation.navigate("DiaryTab", "FinalReport", {diary: this.props?.route?.params.diary})}/>);
    }

    computeDays() {
        let days = this.props?.route?.params.diary.challenge.duration;
        return days;
    }

    render() {
        const selfCommitment = this.props?.route?.params.diary.selfCommitment;
        const days = this.computeDays();

        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={styles.progressBarText}>1 von 3</Text>
                <ProgressBar progress={0.33} style={styles.progressBar}/>
                <Paragraph>Du hast Dich {days} Tage Deinem Self-Commitment:</Paragraph>
                <Paragraph style={styles.selfCommitmentParagraph}>"{selfCommitment}"</Paragraph>
                <Paragraph>gestellt! Super, dass Du durchgehalten hast! Es gab Faktoren, die Deinem Self-Commitment in
                    die Quere kamen. Diese Erfahrungen sind sehr wertvoll – sie unterstützen Dich und andere in ihren
                    Self-Commitments für eine nachhaltigere und gerechtere Welt. Du hast auf diese Weise dazu
                    beigetragen, herauszufinden, welche Massnahmen vielverprechend erscheinen, um Nachhaltigkeit zu
                    erreichen.</Paragraph>
                <Paragraph/>
                <Paragraph>Wie ist es Dir ergangen mit Deinem Self-Commitment? Vergleiche mit Deinen Annahmen zu
                    Beginn.</Paragraph>
                <Paragraph/>
                {this.navigateToFinalReportButton()}
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    progressBarText: {
        textAlign: 'center'
    },
    progressBar: {
        marginBottom: 20
    },
    selfCommitmentParagraph: {
        textAlign: "center",
        paddingTop: 20,
        paddingBottom: 20
    }
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FinalReportIntroScreen);