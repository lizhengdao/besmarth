import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, TouchableOpacity, ScrollView, SectionList} from 'react-native';
import {Surface, Text, Paragraph, FAB, ProgressBar, Divider} from "react-native-paper";
import Navigation from "../services/Navigation";
import {Colors} from "../styles/index";
import {showToast} from "../services/Toast";
import {createFactor, createFinalMonitoring, createHypothesis} from "../services/DiaryService";

class FinalReportFactorsScreen extends React.Component {

    diaryData = {};

    constructor(props) {
        super(props);
        this.state = {
            selected: [],
        };
        this.diaryData = this.props?.route?.params.diary;
    }

    addItemsToSelected(item) {
        if (!this.state.selected.includes(item)) {
            if (this.state.selected.length === 5) {
                alert("Maximal 5 Elemente anwählbar!")
                return
            }
            this.state.selected.push(item)
        } else {
            const index = this.state.selected.indexOf(item);
            this.state.selected.pop(index);
        }
    }

    selectableFactors(item) {
        return (
            <View>
                <TouchableOpacity
                    style={this.state.selected.includes(item) ? styles.selectedItem : styles.item}
                    onPress={() => this.addItemsToSelected(item)}>
                    <Text>{item.text}</Text>
                </TouchableOpacity>
            </View>);
    }

    getFactorList() {
        const factorList = [
            {
                title: "Erleichternd",
                data: []
            },
            {
                title: "Erschwerend",
                data: []
            }
        ];
        let factorsData = this.diaryData.factors.filter(factor => factor.finalFactor === false);
        factorsData.forEach(factor => {
            if (factor.positive) {
                let factorsForList = {
                    text: "👍: " + factor.text,
                    positive: true
                };
                factorList[0].data.push(factorsForList);
            } else {
                let factorsForList = {
                    text: "👎: " + factor.text,
                    positive: false
                };
                factorList[1].data.push(factorsForList);
            }
        })
        return factorList;
    }

    listPositiveAndNegativePosts() {
        return (
            <ScrollView>
                <Surface style={styles.card}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getFactorList()}
                        renderItem={({item}) => this.selectableFactors(item)}
                        keyExtractor={(item, index) => index}
                        renderSectionHeader={({section: {title}}) => (
                            <Text style={styles.sectionHeader}>{title}</Text>
                        )}
                    />
                </Surface>
            </ScrollView>
        )
    }

    navigateToExperienceOverviewButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Abschliessen"
            accessibilityLabel="Abschliessen"
            onPress={() => this.challengeCompleted()}/>);
    }

    async challengeCompleted() {
        if (this.state.selected.length < 3) {
            alert("Bitte mindestens 3 Elemente anwählen!")
            return
        }
        const factors = this.prepareFactorData();
        const diaryId = this.diaryData.id;
        await createHypothesis(diaryId, this.diaryData.endHypothesises);
        await factors.forEach(factor => {
            createFactor(diaryId, factor);
        })
        await createFinalMonitoring(diaryId);
        Navigation.navigate("DiaryTab", "ExperienceOverview")
        this.props.showToast("Challenge erfolgreich abgeschlossen!");
    }

    prepareFactorData() {
        const factors = [];
        this.state.selected.forEach(factor => {
            let factorText = factor.text;
            factorText = factorText.substring(3)
            let minimalFactor = {
                text: factorText,
                positive: factor.positive,
                finalFactor: true
            };
            factors.push(minimalFactor);
        })
        return factors;
    }

    render() {
        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>3 von 3</Text>
                <ProgressBar progress={1.0} style={{marginBottom: 20}}/>
                <Paragraph>Was beeinflusste Dich bei der Einlösung Deines Self-Commitments? Wähle die <Text
                    style={{fontWeight: 'bold'}}>drei bis fünf</Text> wichtigsten Faktoren:</Paragraph>
                <Paragraph/>
                <Divider/>
                <Paragraph/>
                {this.listPositiveAndNegativePosts()}
                <Paragraph/>
                {this.navigateToExperienceOverviewButton()}
                <Paragraph/>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    sectionHeader: {
        fontSize: 15,
        fontWeight: 'bold',
    },
    item: {
        fontSize: 14,
    },
    selectedItem: {
        fontSize: 14,
        backgroundColor: Colors.lightGreen,
        borderRadius: 10,
        padding: 10,
        margin: 2,
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 5,
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({
    showToast
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FinalReportFactorsScreen);