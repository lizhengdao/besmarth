import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {ScrollView, SectionList, StyleSheet, View} from 'react-native';
import {
    Headline,
    Text,
    Paragraph,
    Button,
    FAB,
    TextInput,
    ProgressBar,
    Surface,
    Divider
} from "react-native-paper";
import Navigation from "../services/Navigation";
import {Typography} from "../styles";
import {Ionicons} from "@expo/vector-icons";
import {Colors} from "../styles/index";
import {showToast} from "../services/Toast";
import {createPost, createDailyMonitoring} from "../services/DiaryService";

class DailyReflectionScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            post: "",
            postList: [
                {
                    title: "Erleichternd",
                    data: []
                },
                {
                    title: "Erschwerend",
                    data: []
                }
            ],
            minimalPostList: []
        };
    }

    navigateToExperienceOverviewButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Abschliessen"
            accessibilityLabel="Abschliessen"
            onPress={() => this.dailyFinished()}/>);
    }

    async dailyFinished() {
        let posts = this.state.minimalPostList;
        let diaryId = this.props?.route?.params.diary.id;
        await posts.forEach(post => {
            createPost(diaryId, post);
        })
        await createDailyMonitoring(diaryId, this.getDailyMonitoringData());
        Navigation.navigate("DiaryTab", "ExperienceOverview")
        this.props.showToast("Tägliche Selbstbeobachtung abgeschlossen!");
    }

    getDailyMonitoringData() {
        let diaryData = this.props?.route?.params;
        let dailyMonitoring = {};
        dailyMonitoring.wellbeing = diaryData.wellbeing;
        dailyMonitoring.done = diaryData.challengeCompleted;
        return dailyMonitoring;
    }

    onChangeField(fieldName, newValue) {
        this.setState({
            ...this.state,
            [fieldName]: newValue
        });
    }

    ItemSeparator = () => {
        return (
            <View style={styles.itemSeparator}/>
        );
    }

    postListView() {
        if(this.state.minimalPostList.length !== 0) {
            return (
            <ScrollView>
                <Surface style={styles.card}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.state.postList}
                        keyExtractor={(item, index) => item + index}
                        renderItem={({item}) => <Text style={styles.item}>{item.text}</Text>}
                        renderSectionHeader={({section: {title}}) => (
                            <Text style={styles.sectionHeader}>{title}</Text>
                        )}
                    />
                </Surface>
            </ScrollView>
            )
        }
    }

    addPositivePost() {
        //Add minimalPost for backend
        let minimalPost = {};
        minimalPost.text = this.state.post;
        minimalPost.positive = true;
        this.state.minimalPostList.push(minimalPost);
        //Add post for frontend to postList
        let post = {};
        post.text = "👍: " + this.state.post;
        let postList = this.state.postList;
        postList[0].data.push(post);
        this.setState({...this.state, post: "", postList: postList});
    }

    addNegativePost() {
        //Add minimalPost for backend
        let minimalPost = {};
        minimalPost.text = this.state.post;
        minimalPost.positive = false;
        this.state.minimalPostList.push(minimalPost);
        //Add post for frontend to postList
        let post = {};
        post.text = "👎: " + this.state.post;
        let postList = this.state.postList;
        postList[1].data.push(post);
        this.setState({...this.state, post: "", postList: postList});
    }

    writeDailyPostsView() {
        return (
            <View style={styles.row}>
                <TextInput
                    style={styles.formControl}
                    label="Post eingeben"
                    value={this.state.post}
                    accessibilityLabel="Post eingeben"
                    onChangeText={val => this.onChangeField("post", val)}
                />
                <Button disabled={this.state.post === ""} onPress={() => this.addPositivePost()}><Ionicons
                    style={styles.rightIcon} name={"ios-thumbs-up"} size={25}/></Button>
                <Button disabled={this.state.post === ""} onPress={() => this.addNegativePost()}><Ionicons
                    style={styles.rightIcon} name={"ios-thumbs-down"} size={25}/></Button>
            </View>
        );
    }

    render() {
        const dailyObservationText = "Was hat Dich heute in Deinem Self-Commitment unterstützt? Was hat es Dir heute erschwert, Dein Self-Commitment einzulösen?"
        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>2 von 2</Text>
                <ProgressBar progress={1.0} style={{marginBottom: 20}}/>
                <Paragraph>{dailyObservationText}</Paragraph>
                <Divider style={{margin: 15}}/>
                <Headline style={Typography.descTitle}>Deine Posts von Heute</Headline>
                <Paragraph>Poste hier kurze Texte, die zeigen, was Dich in Deinem
                    Self-Commitment unterstützt hat und was nicht (z.B. Produkteauswahl, Verhalten anderer Menschen,
                    Zeitungsberichte, usw.)</Paragraph>
                {this.postListView()}
                {this.writeDailyPostsView()}
                {this.navigateToExperienceOverviewButton()}
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center"
    },
    formControl: {
        marginVertical: 10,
        width: 250
    },
    sectionHeader: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    item: {
        fontSize: 14,
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 5,
    },
    itemSeparator: {
        padding: 2
    }
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({
    showToast
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DailyReflectionScreen);