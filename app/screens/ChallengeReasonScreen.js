import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, ScrollView} from 'react-native';
import {FAB, Paragraph, TextInput, ProgressBar, Text} from "react-native-paper";
import Navigation from "../services/Navigation";
import {Colors} from  "../styles/index";

class ChallengeReasonScreen extends React.Component {

    diaryData = {};

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            selfCommitment_reason: ""
        };
        this.diaryData = this.props?.route?.params;
    }

    nextButton() {
        this.diaryData.selfCommitment_reason = this.state.selfCommitment_reason;
        return (<FAB
            style={(this.state.selfCommitment_reason !== "") ? styles.button : styles.buttonGrey}
            disabled={(this.state.selfCommitment_reason === "")}
            mode="contained"
            label="Weiter"
            accessibilityLabel="Zur Hypothese"
            onPress={() => Navigation.navigate("HomeTab", "ChallengeHypothesis", {
                diaryData:  this.diaryData
            })}/>);
    }

    onTextInputChange(value) {
        this.setState({...this.state, selfCommitment_reason: value});
    }

    render() {
        return (
            <ScrollView style={{padding: 10}}>
                <Text style={{textAlign: 'center'}}>1 von 3</Text>
                <ProgressBar progress={0.33} style={{marginBottom: 20}}/>
                <Paragraph style={{padding: 10}}>Warum hast Du dieses Self-Commitment gewählt?</Paragraph>
                <TextInput style={{padding: 0}}
                           label="Begründung des Self Commitments"
                           accessibilityLabel="Begründung des Self Commitments"
                           multiline={true}
                            onChangeText={value => {
                                this.onTextInputChange(value);
                            }}/>
                {this.nextButton()}
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    buttonGrey: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.lightGray,
    }
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ChallengeReasonScreen);