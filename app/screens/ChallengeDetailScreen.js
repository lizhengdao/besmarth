import React from "react";
import {ScrollView, StyleSheet, View} from "react-native";
import {bindActionCreators} from "redux";
import {
    changeSharedStateChallenge,
    createChallengeProgress,
    fetchChallengeParticipation,
    getNumberOfCompletedChallenges,
    progressButtonAvailable,
    unfollowChallenge,
    fetchChallenges
} from "../services/Challenges";
import {connect} from "react-redux";
import {notNull} from "services/utils";
import {ActivityIndicator, Card, FAB, Paragraph, Text} from "react-native-paper";
import {showToast} from "services/Toast";
import {ChallengeProgress} from "components/Challenge/ChallengeProgress";
import {ChallengeInfo} from "components/Challenge/ChallengeInfo";
import {ChallengeDataTable} from "components/Challenge/ChallengeDataTable";
import {ChallengeEdit} from "components/Challenge/ChallengeEdit";
import {Buttons} from  "styles/index";
import {Picker} from "react-native";
import Navigation from "../services/Navigation";
import {Colors} from  "../styles/index";

class ChallengeDetailScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            selectedSelfCommitment: "",
            infotrailerAccepted: false,
        };
    }

    componentDidMount() {
        this.reload().then();
    }

    async reload() {
        try {
            const challengeId = this.props?.route?.params?.challenge.id;
            const challenges = await fetchChallenges(this.props.topicId, this.props.userId);
            const challenge = challenges.find(c => {
                return c.id === challengeId
            });
            this.props.navigation.setOptions({
                title: challenge?.title
            });
            this.setState({loading: true, challenge});
            const participation = await fetchChallengeParticipation(challenge);
            this.setState({loading: false, challenge, participation});
            this.setState({...this.state, selectedSelfCommitment: challenge.selfCommitment.self_commitment1})
        } catch (e) {
            console.log(e);
            this.setState({loading: false, error: "Ein Fehler ist aufgetreten"});
        }
    }

    buildLogProgressButton() {
        if (progressButtonAvailable(this.state.participation)) {
            return (
                <FAB
                    style={Buttons.challengeFab}
                    label="Geschafft"
                    mode="contained"
                    accessibilityLabel="Challenge als erledigt markieren"
                    onPress={this.onLogProgress.bind(this)}/>
            );
        } else {
            return null;
        }
    }

    async onLogProgress() {
        this.setState({
            ...this.state,
            participation: await createChallengeProgress(this.state.challenge)
        });

        if (this.state.participation.progress.length % this.state.challenge.duration === 0) {
            this.props.showToast("Gratuliere! Du hast die Challenge " + this.state.challenge.title + " erfolgreich abgeschlossen");
            this.reload();
        }
    }

    async unfollowChallenge() {
        this.setState({
            ...this.state,
            participation: await unfollowChallenge(this.state.challenge)
        });
        this.props.showToast("Challenge deabonniert.");
        this.reload();
    }

    async onShareChallenge() {
        this.setState({
            ...this.state,
            participation: await changeSharedStateChallenge(this.state.challenge, true)
        });
        this.props.showToast("Deine Freunde können deine Fortschritt dieser Challenge nun sehen");
    }

    async onUnshareChallenge() {
        this.setState({
            ...this.state,
            participation: await changeSharedStateChallenge(this.state.challenge, false)
        });
        this.props.showToast("Deine Freunde können deinen Fortschritt nicht mehr sehen.");
    }

    /**
     * Returns true if the user has already signed up for this challenge
     */
    isChallengeAlreadyStarted() {
        return notNull(this.state.participation) && notNull(this.state.participation?.joined_time) && !this.state.participation?.mark_deleted;
    }

    buildSharingButton() {
        if (this.state.participation?.shared) {
            return (
                <FAB
                    label="Teilen aufheben"
                    accessibilityLabel="Teilen aufheben"
                    style={styles.button}
                    mode="outlined"
                    onPress={this.onUnshareChallenge.bind(this)}
                />
            )
        } else {
            return (
                <FAB
                    label="Mit Freunden teilen"
                    accessibilityLabel="Mit Freunden teilen"
                    style={styles.button}
                    mode="outlined"
                    onPress={this.onShareChallenge.bind(this)}
                />
            )
        }
    }

    // Change challenge duration with the given value from the ChallengeEdit MotionSlider
    handleChallengeDurationChange = value => {
        this.setState({...this.state, challenge: {...this.state.challenge, duration: value}});
    }

    selfCommitmentPickerItem1() {
        let selfCommitments = this.state.challenge.selfCommitment;
        if (selfCommitments.self_commitment1 !== "") {
            return (
                <Picker.Item label={selfCommitments.self_commitment1} value={selfCommitments.self_commitment1}/>
            )
        }
    }

    selfCommitmentPickerItem2() {
        let selfCommitments = this.state.challenge.selfCommitment;
        if (selfCommitments.self_commitment2 !== "") {
            return (
                <Picker.Item label={selfCommitments.self_commitment2} value={selfCommitments.self_commitment2}/>
            )
        }
    }

    selfCommitmentPickerItem3() {
        let selfCommitments = this.state.challenge.selfCommitment;
        if (selfCommitments.self_commitment3 !== "") {
            return (
                <Picker.Item label={selfCommitments.self_commitment3} value={selfCommitments.self_commitment3}/>
            )
        }
    }

    selfCommitmentPickerItem4() {
        let selfCommitments = this.state.challenge.selfCommitment;
        if (selfCommitments.self_commitment4 !== "") {
            return (
                <Picker.Item label={selfCommitments.self_commitment4} value={selfCommitments.self_commitment4}/>
            )
        }
    }

    selfCommitmentPickerItem5() {
        let selfCommitments = this.state.challenge.selfCommitment;
        if (selfCommitments.self_commitment5 !== "") {
            return (
                <Picker.Item label={selfCommitments.self_commitment5} value={selfCommitments.self_commitment5}/>
            )
        }
    }

    selectSelfCommitment() {
        const duration = this.state.challenge.duration;
        let text = "die nächsten " + duration + " Tage";
        return (
            <View style={{padding: 10}}>
                <Paragraph>Mein Self-Commitment für {text}:</Paragraph>
                <Picker
                    selectedValue={this.state.selectedSelfCommitment}
                    style={{height: 44}}
                    itemStyle={{height: 44}}
                    onValueChange={(itemValue) =>
                        this.setState({selectedSelfCommitment: itemValue})
                    }>
                    {this.selfCommitmentPickerItem1()}
                    {this.selfCommitmentPickerItem2()}
                    {this.selfCommitmentPickerItem3()}
                    {this.selfCommitmentPickerItem4()}
                    {this.selfCommitmentPickerItem5()}
                </Picker>
            </View>);
    }

    navigateEitherToInfotrailerOrDirectlyToReason() {
        if (this.state.infotrailerAccepted) {
            Navigation.navigate("HomeTab", "ChallengeReason", {selectedSelfCommitment: this.state.selectedSelfCommitment, challenge: this.state.challenge})
        } else {
            Navigation.navigate("HomeTab", "Infotrailer", {selectedSelfCommitment: this.state.selectedSelfCommitment, challenge: this.state.challenge})
        }
    }

    nextButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Weiter"
            accessibilityLabel="Zur Begründung"
            onPress={() => this.navigateEitherToInfotrailerOrDirectlyToReason()}/>);
    }

    render() {
        if (this.state.loading) {
            return <ActivityIndicator size="large"/>;
        }
        if (this.state.error) {
            return <Text>{this.state.error}</Text>;
        }

        let progressOrSignInView;
        if(this.isChallengeAlreadyStarted()) {
            progressOrSignInView = (
                <View>
                <ChallengeDataTable challenge={this.state.challenge}/>
                <Card>
                    <Card.Title title="Dein Fortschritt"/>
                    <Card.Content>
                        <ChallengeProgress participation={this.state.participation}
                                           challenge={this.state.challenge}></ChallengeProgress>
                        {this.buildLogProgressButton()}
                    </Card.Content>
                </Card>
                <Card style={styles.challengeCard}>
                    <Card.Content>
                        {ChallengeProgress.countCompletedProgress(getNumberOfCompletedChallenges(this.state.participation, this.state.challenge))}
                    </Card.Content>
                </Card>
                <Card>
                    <Card.Content>
                        <FAB
                            style={styles.button}
                            label="Deabonnieren"
                            mode="contained"
                            accessibilityLabel="Challenge deabonnieren"
                            testID="Challenge deabonnieren"
                            onPress={this.unfollowChallenge.bind(this)}/>
                        {this.buildSharingButton()}
                    </Card.Content>
                </Card>
            </View>
            );
        } else {
            progressOrSignInView = (
                <ScrollView style={styles.card}>
                <ChallengeEdit challenge={this.state.challenge} onChallengeDurationChange={this.handleChallengeDurationChange}/>
                {this.selectSelfCommitment()}
                <Paragraph/>
                {this.nextButton()}
                <Card style={styles.challengeCard}>
                    <Card.Content>
                        {ChallengeProgress.countCompletedProgress(getNumberOfCompletedChallenges(this.state.participation, this.state.challenge))}
                    </Card.Content>
                </Card>
            </ScrollView>);
        }

        return (
            <View style={styles.container}>
                <ScrollView>
                    <ChallengeInfo challenge={this.state.challenge}/>
                    {progressOrSignInView}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
        color: Colors.white
    },
    challengeCard: {
        borderStyle: "solid",
        borderColor: "white",
        borderWidth: 1,
    }
});

const mapStateToProps = state => {
    const topicId = state.topics.currentTopicId
    const currentTopicName = state.topics.currentTopicName
    let userId;
    if(state.account.account.id === undefined) {
        userId = state.account.account.user_id;
    } else {
        userId = state.account.account.id;
    }
    return {topicId, currentTopicName, userId};
};
const mapDispatchToProps = dispatch => bindActionCreators({
    showToast,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ChallengeDetailScreen);