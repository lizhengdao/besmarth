import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, ScrollView, Slider} from 'react-native';
import {FAB, Surface, Paragraph, ProgressBar, Text, Divider} from "react-native-paper";
import Navigation from "../services/Navigation";
import {Colors} from "../styles/index";

class ChallengeHypothesisScreen extends React.Component {

    diaryData = {};

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            hypothesisDifficulty: 0.50,
            hypothesisLimitation: 0.50,
            hypothesisEnvironment: 0.50,
            hypothesisContribution: 0.50,
            hypothesisBurden: 0.50
        };
        this.diaryData = this.props?.route?.params.diaryData;
    }

    handler = (propName) => (value) => {
        this.setState({
            ...this.state,
            [propName]: value
        });
    }

    sliderOptions(negative, positive) {
        return (
            <View style={{...styles.row}}>
                <Text>{negative}</Text>
                <Text>{positive}</Text>
            </View>);
    }

    hypothesisSliders() {
        return (<View>
            <Surface style={styles.card}>
                <Paragraph>Die Einlösung dieses Self-Commitments wird für mich</Paragraph>
                {this.sliderOptions("sehr schwierig", "sehr leicht")}
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    step={0.1}
                    value={this.state.hypothesisDifficulty}
                    onSlidingComplete={this.handler("hypothesisDifficulty")}
                />
            </Surface>

            <Surface style={styles.card}>
                <Paragraph>Dieses Self-Commitment wird mich</Paragraph>
                {this.sliderOptions("stark einschränken", "befreien")}
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    step={0.1}
                    value={this.state.hypothesisLimitation}
                    onSlidingComplete={this.handler("hypothesisLimitation")}
                />
            </Surface>

            <Surface style={styles.card}>
                <Paragraph>Mein Umfeld wird auf mein Self-Commitment</Paragraph>
                {this.sliderOptions("negativ reagieren", "positiv reagieren")}
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    step={0.1}
                    value={this.state.hypothesisEnvironment}
                    onSlidingComplete={this.handler("hypothesisEnvironment")}
                />
            </Surface>

            <Surface style={styles.card}>
                <Paragraph>
                    Mit Blick auf eine nachhaltigere und gerechtere Welt kann dieses Self-Commitment</Paragraph>
                {this.sliderOptions("nichts bewegen", "einen grossen Beitrag leisten")}
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    step={0.1}
                    value={this.state.hypothesisContribution}
                    onSlidingComplete={this.handler("hypothesisContribution")}
                />
            </Surface>

            <Surface style={styles.card}>
                <Paragraph>Dieses Self-Commtment wird</Paragraph>
                {this.sliderOptions("mich belasten", "mir gut tun")}
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    step={0.1}
                    value={this.state.hypothesisBurden}
                    onSlidingComplete={this.handler("hypothesisBurden")}
                />
            </Surface>
        </View>);
    }

    nextButton() {
        this.diaryData.startHypothesis = {
            difficulty: this.state.hypothesisDifficulty.toFixed(2),
            limitation: this.state.hypothesisLimitation.toFixed(2),
            environment: this.state.hypothesisEnvironment.toFixed(2),
            contribution: this.state.hypothesisContribution.toFixed(2),
            burden: this.state.hypothesisBurden.toFixed(2)
        }
        this.diaryData.challenge.user = this.props.userId;
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Weiter"
            accessibilityLabel="Zur Hypothese"
            onPress={() => Navigation.navigate("HomeTab", "ChallengePushSetting", {
                diaryData: this.diaryData
            })}/>);
    }

    render() {
        let selfCommitment = this.diaryData.selectedSelfCommitment;
        return (
            <ScrollView style={styles.container}>
                <Text style={{textAlign: 'center'}}>2 von 3</Text>
                <ProgressBar progress={0.66} style={{marginBottom: 20}}/>
                <Paragraph style={{textAlign: "center", paddingBottom: 10}}>Was denkst Du, wie wird es Dir ergehen mit
                    deinem
                    Self-Commitment: "{selfCommitment}"?</Paragraph>
                <Divider/>
                {this.hypothesisSliders()}
                {this.nextButton()}
                <Paragraph/>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    row: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingTop: 15,
    },
    container: {
        flex: 1,
        padding: 10,
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        marginBottom: 10,
        backgroundColor: Colors.green,
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 10,
    },
    slider: {
        padding: 20
    }
});

const mapStateToProps = state => {
    let userId;
    if(state.account.account.id === undefined) {
        userId = state.account.account.user_id;
    } else {
        userId = state.account.account.id;
    }
    return {userId};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ChallengeHypothesisScreen);