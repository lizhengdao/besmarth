import React from "react";
import {Image, StyleSheet, View} from "react-native";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Button, Text} from "react-native-paper";
import Navigation from "../services/Navigation";
import TopicLevel from "../components/TopicLevel";

class TopicScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    this.props.navigation.setOptions({title: this.props.currentTopicName});
  }

  navigateToChallenges(topicId) {
    Navigation.push("HomeTab", "Challenges", {topicName: this.props.currentTopicName});
  }

  render() {
    const image = this.props.currentTopicImage;
    return (
      <View style={styles.container}>

        <TopicLevel topicId={this.props.currentTopicId} size="60" textColor="black" showProgressBar />

        <View style={styles.containerTop}>
          <Image source={{uri: image}} style={styles.topicImage}/>
          <Text style={styles.desc}>{this.props.currentTopicDescription}</Text>
        </View>

        <View>
          <Button style={styles.button} mode="contained"
                  onPress={() => this.navigateToChallenges(this.props.currentTopicId)}>
            {this.props.currentTopicName} Challenges
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  containerTop: {
    flex: 1,
  },
  containerBottom: {
    flex: 1,
  },
  button: {
    marginVertical: 80,
    minWidth: "50%"
  },
  topicImage: {
    borderRadius: 15,
    marginTop: 50,
    position: "relative",
    height: "100%",
    width: "100%",
  },
  desc: {
    position: "absolute",
    width: "100%",
    textAlign: "center",
    fontSize: 16,
  },
});

const mapStateToProps = state => {
  return {
    currentTopicId: state.topics.currentTopicId,
    currentTopicName: state.topics.currentTopicName,
    currentTopicDescription: state.topics.currentTopicDescription,
    currentTopicImage: state.topics.currentTopicImage
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TopicScreen);
