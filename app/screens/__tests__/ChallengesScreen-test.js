import React from "react";
import ChallengesScreen from "../ChallengesScreen";
import {createEmptyTestStore, createNetworkMock, renderScreenForTest} from "../../test/test-utils";
import {fireEvent, waitForElement, waitForElementToBeRemoved} from "@testing-library/react-native";

const mock = createNetworkMock();

describe("ChallengesScreen", () => {

  it("renders empty challenge list", async () => {
    mock.reset();
    mock.onGet("/challenges/topic/1/").reply(200, []);

    const store = createEmptyTestStore();
    store.dispatch({type: "CHANGE_CURRENT_TOPIC", item: {id: 1, name: "Topicname"}});

    const {getByTestId, asJSON} = renderScreenForTest(ChallengesScreen, store);
    await waitForElement(() => getByTestId("challenge-list"));
    expect(asJSON()).toMatchSnapshot();
  });

  it(`renders with two challenges`, async () => {
    mock.reset();
    mock.onGet("/challenges/topic/1/").reply(200, [
      {
        id: 1,
        title: "Challenge Title",
        image: "http://test.local/image/url",
        color: "#abcdef"
      },
      {
        id: 2,
        title: "Challenge Title 2",
        image: "http://test.local2222/image/url",
        color: "#abc22f"
      }
    ]);

    const store = createEmptyTestStore();
    store.dispatch({type: "CHANGE_CURRENT_TOPIC", item: {id: 1, name: "Topicname"}});
    const {getByTestId, asJSON} = renderScreenForTest(ChallengesScreen, store);
    await waitForElement(() => getByTestId("challenge-list"));
    expect(asJSON()).toMatchSnapshot();
  });

  it(`challenges filter filters challenges screen`, async () => {
    mock.reset();
    mock.onGet("/challenges/topic/1/").reply(200, [
      {
        id: 1,
        title: "Lorem ipsum",
        image: "http://test.local/image/url",
        color: "#abcdef"
      },
      {
        id: 2,
        title: "dolurs amuts",
        image: "http://test.local2222/image/url",
        color: "#abc22f"
      }
    ]);

    const store = createEmptyTestStore();
    store.dispatch({type: "CHANGE_CURRENT_TOPIC", item: {id: 1, name: "Topicname"}});
    const {getByTestId, getByLabelText, getByText, asJSON} = renderScreenForTest(ChallengesScreen, store);

    await waitForElement(() => getByTestId("challenge-list"));
    fireEvent.press(getByTestId("toggle-challenges-filter"));
    expect(asJSON()).toMatchSnapshot();

    // search for 'ipsum', which should filter out the second challenge
    const searchField = await waitForElement(() => getByLabelText("Suchbegriff"));
    fireEvent.changeText(searchField, "ipsum");
    await waitForElementToBeRemoved(() => getByLabelText("dolurs amuts"));
    expect(asJSON()).toMatchSnapshot();

    // reset filter, now the second challenge should reappear
    fireEvent.press(getByLabelText("Filter zurücksetzen"));
    await waitForElement(() => getByLabelText("dolurs amuts"));
    expect(asJSON()).toMatchSnapshot();
  });
});
