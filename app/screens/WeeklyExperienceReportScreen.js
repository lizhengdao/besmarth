import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, ScrollView} from 'react-native';
import {Headline, Text, Paragraph, Button, FAB, ProgressBar, Divider} from "react-native-paper";
import Navigation from "../services/Navigation";
import {Ionicons} from "@expo/vector-icons";
import {LineChart} from "react-native-chart-kit";
import {Dimensions} from "react-native";
import {Colors, Typography} from "../styles/index";

class WeeklyExperienceReportScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true
        };
    }

    navigateToWeeklyFactorsOverviewButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Weiter"
            accessibilityLabel="Weiter"
            onPress={() => Navigation.navigate("DiaryTab", "WeeklyFactorsOverview", {data: this.props?.route?.params})}/>);
    }

    getWellBeingData() {
        let dailyMonitorings = this.props?.route?.params.diary.daily_monitorings;
        let wellBeingData = [];
        dailyMonitorings.forEach(dailyMonitoring => {
            wellBeingData.push(parseFloat(dailyMonitoring.wellbeing))
        })
        return wellBeingData;
    }

    getWeekdaysDoneData() {
        let dailyMonitorings = this.props?.route?.params.diary.daily_monitorings;
        let doneData = [];
        dailyMonitorings.forEach(dailyMonitoring => {
            doneData.push(dailyMonitoring.done)
        })
        doneData.forEach(element => {
            if(element) {
                doneData[doneData.indexOf(element)] = "ios-checkmark-circle"
            } else {
                doneData[doneData.indexOf(element)] = "ios-close-circle"
            }
        })
        return (
            <View style={{...styles.row}}>
                <Button><Ionicons style={styles.rightIcon} name={doneData[0]} size={25}/></Button>
                <Button><Ionicons style={styles.rightIcon} name={doneData[1]} size={25}/></Button>
                <Button><Ionicons style={styles.rightIcon} name={doneData[2]} size={25}/></Button>
                <Button><Ionicons style={styles.rightIcon} name={doneData[3]} size={25}/></Button>
                <Button><Ionicons style={styles.rightIcon} name={doneData[4]} size={25}/></Button>
                <Button><Ionicons style={styles.rightIcon} name={doneData[5]} size={25}/></Button>
                <Button><Ionicons style={styles.rightIcon} name={doneData[6]} size={25}/></Button>
            </View>
        )
    }

    weekdaysDoneOverview() {
        return (<View>
                {this.getWeekdaysDoneData()}
                <View style={{...styles.row}}>
                    <Text>Mo</Text>
                    <Text>Di</Text>
                    <Text>Mi</Text>
                    <Text>Do</Text>
                    <Text>Fr</Text>
                    <Text>Sa</Text>
                    <Text>So</Text>
                </View>
            </View>
        )
    }

    weekWellBeing() {
        // TODO: LineChart from https://www.npmjs.com/package/react-native-chart-kit
        const data = {
            labels: ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"],
            datasets: [
                {
                    data: this.getWellBeingData(), // TODO: Problem beginnt einf bei tiefstem wert!
                    color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`, // optional
                    strokeWidth: 2 // optional
                }
            ],
        };
        const screenWidth = Dimensions.get("window").width +100; // TODO: hack to hide yAxis
        const chartConfig = {
            backgroundGradientFrom: "#1E2923",
            backgroundGradientFromOpacity: 0,
            backgroundGradientTo: "#08130D",
            backgroundGradientToOpacity: 0.5,
            color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            strokeWidth: 2, // optional, default 3
            barPercentage: 0.5,
            useShadowColorFromDataset: false // optional
        };
        return (<View style={{...styles.row}}>
            <LineChart
                withInnerLines={false}
                data={data}
                width={screenWidth}
                bezier
                height={220}
                chartConfig={chartConfig}
                style={{marginRight: 8}}
            />
        </View>)
    }

    render() {
        const weekNr = this.props?.route?.params.week

        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>1 von 3</Text>
                <ProgressBar progress={0.33} style={{marginBottom: 20}}/>
                <Headline style={Typography.descTitle}>Woche {weekNr}</Headline>
                <Paragraph>Tage mit eingelöstem Self-Commitment:</Paragraph>
                {this.weekdaysDoneOverview()}
                <Paragraph/>
                <Divider/>
                <Paragraph/>
                <Paragraph>Dein Wohlbefinden mit Deinem Self-Commitment:</Paragraph>
                {this.weekWellBeing()}
                <Paragraph/>
                {this.navigateToWeeklyFactorsOverviewButton()}
                <Paragraph/>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(WeeklyExperienceReportScreen);