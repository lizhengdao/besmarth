import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, ScrollView} from 'react-native';
import {Paragraph, Switch, FAB} from "react-native-paper";
import Navigation from "../services/Navigation";
import {Colors} from "../styles";

class InfotrailerScreen extends React.Component {

    data = {}

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            acceptDataUsage: false, // TODO: In Account vermerken!!!
        };
        this.data = this.props?.route?.params
    }

    onChangeField(fieldName, newValue) {
        this.setState({
            ...this.state,
            [fieldName]: newValue
        });
    }

    navigateToChallengeReason() {
        return (
            <FAB
                style={this.state.acceptDataUsage ? styles.button : styles.buttonGrey}
                disabled={!this.state.acceptDataUsage}
                mode="contained"
                label="Los Geht's"
                accessibilityLabel="Zur Begründung"
                // TODO: send properties of selected challenge to next screen
                onPress={() => Navigation.navigate("HomeTab", "ChallengeReason", {selectedSelfCommitment: this.data.selectedSelfCommitment, challenge: this.data.challenge})}/>);
    }

    render() {
        const infotrailerText = "«Die Vision einer nachhaltigeren und gerechteren Welt erfordert Veränderungen in der Politik, in der Gesellschaft, bei jedem einzelnen Menschen. \n" +
            "\n" +
            "Wo kann sie beginnen? Bei den internationalen Nachhaltigkeitszielen, bei den Unternehmen oder bei uns und in unserem sozialen Umfeld? \n" +
            "\n" +
            "Diese App möchte den Bogen vom persönlichen Engagement auf Zeit zur kulturellen und politischen Veränderung unserer Gesellschaft schlagen. Sie ermöglicht, die persönlichen Erfahrungen eines 30-tägigen Self-Commitments zu nutzen, um Wege zu erkennen, die es für individuelle, gesellschaftliche und politische Veränderungen hin zu einer nachhaltigeren und gerechteren Welt gibt. \n" +
            "\n" +
            "Du entscheidest darüber, mit welchem Self-Commitment Du dabei sein willst. Du bestimmst, ob Deine – mit dieser App gesammelten – Erfahrungen nur von Dir genutzt werden dürfen oder ob Du diese – natürlich anonymisiert - anderen zugänglich machst, damit auch sie daraus lernen und Ideen entwickeln können. Bist Du dabei?"

        return (
            <ScrollView style={styles.contentWrapper}>
                <Paragraph>{infotrailerText}</Paragraph>
                <View style={{...styles.formControl, ...styles.row}}>
                    <Switch value={this.state.acceptDataUsage}
                            accessibilityLabel="Ich stimme der Datennutzung & Verarbeitung zu."
                            onValueChange={val => this.onChangeField("acceptDataUsage", val)}/>
                    <Paragraph>Ich stimme der Datennutzung zu.</Paragraph>
                </View>
                {this.navigateToChallengeReason()}
                <Paragraph/>
            </ScrollView>)
    }
}

const styles = StyleSheet.create({
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center"
    },
    formControl: {
        marginVertical: 10,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    buttonGrey: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.lightGray,
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(InfotrailerScreen);