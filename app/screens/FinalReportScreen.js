import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, ScrollView, Slider} from 'react-native';
import {Text, Paragraph, FAB, ProgressBar, Surface, Divider} from "react-native-paper";
import Navigation from "../services/Navigation";
import {Colors} from "../styles/index";

class FinalReportScreen extends React.Component {

    diaryData = {};

    constructor(props) {
        super(props);
        this.state = {
            hypothesisDifficulty: 0.50,
            hypothesisLimitation: 0.50,
            hypothesisEnvironment: 0.50,
            hypothesisContribution: 0.50,
            hypothesisBurden: 0.50
        };
        this.diaryData = this.props?.route?.params.diary;
    }

    getHypothesisStart() {
        let hypothesisStart;
        hypothesisStart = this.diaryData.hypothesises.find(hypo => hypo.startHypothesis === true && hypo.endHypothesis === false);
        return hypothesisStart;
    }

    sliderOptions(negative, positive) {
        return (
            <View style={{...styles.row}}>
                <Text>{negative}</Text>
                <Text>{positive}</Text>
            </View>);
    }

    navigateToFinalReportFactorsButton() {
        this.diaryData.endHypothesises = {
            startHypothesis: false,
            endHypothesis: true,
            difficulty: this.state.hypothesisDifficulty,
            limitation: this.state.hypothesisLimitation,
            environment: this.state.hypothesisEnvironment,
            contribution: this.state.hypothesisContribution,
            burden: this.state.hypothesisBurden
        };
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Weiter"
            accessibilityLabel="Weiter"
            onPress={() => Navigation.navigate("DiaryTab", "FinalReportFactors", {diary: this.diaryData})}/>);
    }

    handler = (propName) => (value) => {
        this.setState({
            ...this.state,
            [propName]: value
        });
    }

    hypothesisSliders() {
        const startHypothesises = this.getHypothesisStart();
        return (<View>
            <Surface style={styles.card}>
                <Paragraph style={styles.paragraph}>Die Einlösung dieses Self-Commitments war für mich</Paragraph>
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(startHypothesises.difficulty)}
                />
                {this.sliderOptions("sehr schwierig", "sehr leicht")}
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    step={0.1}
                    value={this.state.hypothesisDifficulty}
                    onSlidingComplete={this.handler("hypothesisDifficulty")}
                />
            </Surface>

            <Surface style={styles.card}>
                <Paragraph style={styles.paragraph}>Dieses Self-Commitment hat mich</Paragraph>
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(startHypothesises.limitation)}
                />
                {this.sliderOptions("stark eingeschränkt", "befreit")}
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    step={0.1}
                    value={this.state.hypothesisLimitation}
                    onSlidingComplete={this.handler("hypothesisLimitation")}
                />
            </Surface>

            <Surface style={styles.card}>
                <Paragraph style={styles.paragraph}>Mein Umfeld hat auf mein Self-Commitment</Paragraph>
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(startHypothesises.environment)}
                />
                {this.sliderOptions("negativ reagiert", "positiv reagiert")}
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    step={0.1}
                    value={this.state.hypothesisEnvironment}
                    onSlidingComplete={this.handler("hypothesisEnvironment")}
                />
            </Surface>

            <Surface style={styles.card}>
                <Paragraph style={styles.paragraph}>
                    Mit Blick auf eine nachhaltigere und gerechtere Welt konnte dieses Self-Commitment</Paragraph>
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(startHypothesises.contribution)}
                />
                {this.sliderOptions("nichts bewegen", "einen grossen Beitrag leisten")}
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    step={0.1}
                    value={this.state.hypothesisContribution}
                    onSlidingComplete={this.handler("hypothesisContribution")}
                />
            </Surface>

            <Surface style={styles.card}>
                <Paragraph style={styles.paragraph}>Dieses Self-Commtment hat</Paragraph>
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(startHypothesises.burden)}
                />
                {this.sliderOptions("mich belastet", "mir gut getan")}
                <Slider
                    style={styles.slider}
                    maximumValue={1}
                    minimumValue={0}
                    step={0.1}
                    value={this.state.hypothesisBurden}
                    onSlidingComplete={this.handler("hypothesisBurden")}
                />
            </Surface>
        </View>);
    }

    render() {
        const days = this.diaryData.challenge.duration;
        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>2 von 3</Text>
                <ProgressBar progress={0.66} style={{marginBottom: 20}}/>
                <Paragraph/>
                <Paragraph>So hast Du das Self-Commitment vor {days} Tagen beurteilt, wie beurteilst du es
                    jetzt?</Paragraph>
                <Paragraph/>
                <Divider/>
                {this.hypothesisSliders()}
                {this.navigateToFinalReportFactorsButton()}
                <Paragraph></Paragraph>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    row: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 10,
    },
    paragraph: {
        paddingBottom: 10
    }
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FinalReportScreen);