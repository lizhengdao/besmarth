import React, {useState, useEffect, useRef} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, ScrollView} from 'react-native';
import {FAB, Text, Paragraph, Button, ProgressBar} from "react-native-paper";
import Navigation from "../services/Navigation";
import DateTimePicker from '@react-native-community/datetimepicker';
import {showToast} from "../services/Toast";
import {Colors} from "../styles/index";
import {signUpChallenge} from "../services/Challenges";
import {createDailyMonitoring, createDiary, createHypothesis} from "../services/DiaryService";
import * as Notifications from 'expo-notifications';
import {
    registerForPushNotificationsAsync,
    scheduleDailyPushNotification,
    scheduleWeeklyPushNotification
} from "../services/NotificationService";

export const ChallengePushSettingScreen = (props) => {
    const [expoPushToken, setExpoPushToken] = useState('');
    const [notification, setNotification] = useState(false);
    const notificationListener = useRef();
    const responseListener = useRef();

    useEffect(() => {
        registerForPushNotificationsAsync().then(token => setExpoPushToken(token));

        notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
            setNotification(notification);
        });

        // When user clicks on notification
        responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
            Navigation.navigate("DiaryTab", "ExperienceOverview");
        });

        return () => {
            Notifications.removeNotificationSubscription(notificationListener);
            Notifications.removeNotificationSubscription(responseListener);
        };
    }, []);

    let diaryData = props?.route?.params.diaryData;

    const [time, setTime] = useState(new Date());
    const [show, setShow] = useState(false);

    const onChange = (event, selectedValue) => {
        const selectedTime = selectedValue || time;
        setShow(false);
        setTime(selectedTime);
    };

    const showMode = (currentMode) => {
        setShow(true);
    };

    const showTimepicker = () => {
        showMode('time');
    };

    /**
     * Used to format numbers with only 1 digit (0-9) to look (00-09) when base 10 is defined. base 100: 000-009 usw.
     * @param nr number to be formatted.
     * @param base decides how many digits should be minimum.
     * @returns {string|*} numbers formatted with given base.
     */
    function zeroPad(nr, base) {
        let len = (String(base).length - String(nr).length) + 1;
        return len > 0 ? new Array(len).join('0') + nr : nr;
    }

    const formatTime = (time) => {
        return `${zeroPad(time.getHours(), 10)}:${zeroPad(time.getMinutes(), 10)}`;
    };

    const onSignupChallenge = async () => {
        const participation = await signUpChallenge(diaryData.challenge);
        diaryData.challengeParticipation = participation;
        const diary = await createDiary(prepareDataForDiaryCreation());
        const diaryId = diary.id;
        diaryData.startHypothesis.startHypothesis = true;
        diaryData.startHypothesis.endHypothesis = false;
        await createHypothesis(diaryId, diaryData.startHypothesis);
        //ONLY FOR USABILITY TESTING
        // TODO: Remove
        let testData = [
            {wellbeing: 0.5, done: false},
            {wellbeing: 0.0, done: false},
            {wellbeing: 0.9, done: true},
            {wellbeing: 0.7, done: true},
            {wellbeing: 0.7, done: true},
            {wellbeing: 0.2, done: false},
            {wellbeing: 1.0, done: true}
        ]
        for (let i = 0; i < testData.length; i++) {
            await createDailyMonitoring(diaryId, testData[i]);
        }
        props.showToast("Gratuliere! Du hast die Challenge " + diaryData.challenge.title + " im Tagebuch-Modus gestartet");
    }

    const prepareDataForDiaryCreation = () => {
        let data = {};
        data.selfCommitment = diaryData.selectedSelfCommitment;
        data.selfCommitment_reason = diaryData.selfCommitment_reason
        data.pushNotification_time = time.toTimeString().slice(0, 5);
        data.participation = diaryData.challengeParticipation;
        return data;
    }

    const startChallengeButton = () => {
        return (
            <FAB
                style={time.getHours() < 18 ? styles.buttonGrey : styles.button}
                disabled={time.getHours() < 18}
                mode="contained"
                label="Challenge starten"
                accessibilityLabel="Challenge starten"
                onPress={async () => {
                    await scheduleDailyPushNotification(time.getHours(), time.getMinutes(), diaryData)
                    await scheduleWeeklyPushNotification(time.getDay()+1, time.getHours(), time.getMinutes(), diaryData)
                    await onSignupChallenge();
                    navigate();
                }}/>);
    }

    const navigate = () => {
        Navigation.navigate("HomeTab", "Home")
        Navigation.navigate("DiaryTab", "ExperienceOverview")
    }

    return (
        <ScrollView style={styles.container}>
            <Text style={{textAlign: 'center'}}>3 von 3</Text>
            <ProgressBar progress={1.0} style={{marginBottom: 20}}/>
            <Paragraph>Du bekommst nun in den nächsten 30 Tagen jeden Abend eine
                Push-Nachricht, die Dich daran erinnert, einzugeben, wie es Dir den Tag durch mit Deinem
                Self-Commitment ergangen ist. Mit dieser täglichen Eingabe kannst Du Dein Self-Commitment
                beobachten.</Paragraph>
            <Paragraph>Die Erfahrungen, die Du mit Deinem Self-Commitment machst, ermöglichen Dir und uns,
                Möglichkeiten für individuelle, gesellschaftliche und politische Veränderungen hin zu einer
                nachhaltigeren und gerechteren Welt zu erkennen.</Paragraph>
            <Paragraph>Was ist für Dich eine gute Zeit für die tägliche Nachfrage? (erst ab 18:00 Uhr
                möglich)</Paragraph>

            <View style={{padding: 10}}>
                <View>
                    <Button onPress={showTimepicker} title="bla"
                            accessibilityLabel="Zeit einstellen">{formatTime(time)}</Button>
                </View>
                {show && (
                    <DateTimePicker
                        testID="dateTimePicker"
                        value={time}
                        mode={"time"}
                        is24Hour={true}
                        display="default"
                        onChange={onChange}
                    />)}
            </View>
            {startChallengeButton()}
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    buttonGrey: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.lightGray,
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({
    showToast
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ChallengePushSettingScreen);