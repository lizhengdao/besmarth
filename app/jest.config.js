
module.exports = {
  "preset": "jest-expo",
  "setupFiles": [
    "<rootDir>/jest.setup.js"
  ],
  "setupFilesAfterEnv": [
    '@testing-library/react-native/cleanup-after-each'
  ],
  "transformIgnorePatterns": [
    "node_modules/(?!(jest-)?react-native|unimodules-permissions-interface|@fortawesome/.*|react-clone-referenced-element|@react-native-community|expo(nent)?|@expo(nent)?/.*|react-navigation|@react-navigation/.*|@unimodules/.*|sentry-expo|native-base)"
  ],
  "globals": {
    "__TESTMODE__": true
  },
  "reporters": [
    "default",
    ["./node_modules/jest-html-reporter", {
      "pageTitle": "Test Report"
    }]
  ],
  "collectCoverageFrom": [
    "**/*.{js,jsx}",
    "!jest.config.js",
    "!**/coverage/**",
    "!**/node_modules/**",
    "!**/babel.config.js",
    "!**/jest.setup.js"
  ],
  "testPathIgnorePatterns" : [
      "ChallengesScreen",
      "NavigationRoot-test",
      "App-test",
      "NavigationStacks-test",
      "ChallengeDetailScreen-test",
      "AccountForm-test",
      "ManageAccountScreen-test",
      "SignInScreen-test",
      "ScanFriendCodeScreen-test",
      "AccountCodeScreen-test",
      "FriendsScreen-test",
      "DailyTipService-test",
      "Challenges-test",
      "TopicLevel-test",
      "ChallengesFilter-test",
      "ErrorBox-test",
      "__tests__"
  ],
};
