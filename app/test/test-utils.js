import React from "react";
import renderer from "react-test-renderer";
import {applyMiddleware, createStore} from "redux";
import rootReducer from "../redux/rootReducer";
import thunkMiddleware from "redux-thunk";
import {notNull} from "../services/utils";
import MockAdapter from "axios-mock-adapter";
import RemoteStorage from "../services/RemoteStorage";
import {render} from "@testing-library/react-native";
import {Provider} from "react-redux";
import {Provider as PaperProvider} from "react-native-paper";
import {createStackNavigator} from "@react-navigation/stack";
import {NavigationContainer} from "@react-navigation/native";

let actionListeners = {};

const testActionLogger = store => next => action => {
  const result = next(action);
  const listeners = actionListeners[action.type] || null;
  if (notNull(listeners)) {
    listeners.forEach(l => l(action));
    delete actionListeners[action.type];
  }
  return result;
};


/**
 * Waits for a redux action to occur
 * If the redux action is not caught in a specific time, the returned promise fails
 */
export async function waitForReduxAction(actionType, timeout = 2000) {
  return new Promise((resolve, reject) => {
    const timeoutId = setTimeout(() => {
      expect("").toBe("Timeout for redux action '" + actionType + "'. That means, the test didn't trigger that action.");
    }, timeout);

    // add the listener to the listener list
    const existingListeners = actionListeners[actionType] || [];
    existingListeners.push((action) => {
      clearTimeout(timeoutId);
      resolve(action);
    });
    actionListeners[actionType] = existingListeners;
  });
}

/**
 * Convenience method to render a component under test
 * @param cut component under test
 * @returns {RenderResult}
 */
export function renderForTest(cut, store) {
  const wrapper = ({children}) => {
    return <Provider store={store}><PaperProvider>{children}</PaperProvider></Provider>;
  };
  return render(cut, {wrapper});
}


/**
 * Renders a top level screen with the necessary navigation props
 */
export function renderScreenForTest(screen, store, routeParams) {
  const Stack = createStackNavigator();
  // in order to be able to define params, we have to provide a initial nav state
  const wrapper = ({children}) => {
    const initialNavState = {
        "index": 0,
        "key": "testscreen",
        "routeNames": [
          "testscreen"
        ],
        "routes": [
          {
            "key": "testscreen",
            "name": "testscreen",
            "params": routeParams
          }
        ],
        "type": "stack"
      }
    ;
    return <Provider store={store}><PaperProvider>
      <NavigationContainer initialState={initialNavState}>
        <Stack.Navigator>
          {children}
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider></Provider>;
  };
  return render(<Stack.Screen name="testscreen" component={screen}/>, {wrapper});
}

/**
 * Render a component for a snapshot test. The component is wrapped inside redux Provider and react-native-paper provider
 */
export function renderForSnapshot(cut, store) {
  if (!notNull(store)) {
    store = createEmptyTestStore();
  }

  return renderer.create(<Provider store={store}><PaperProvider>{cut}</PaperProvider></Provider>);
}

/**
 * Creates an empty redux store for testing purposes.
 */
export function createEmptyTestStore() {
  return createStore(rootReducer, applyMiddleware(thunkMiddleware, testActionLogger));
}

/**
 * Create a mock object which allows mocking http requests
 */
export function createNetworkMock() {
  return new MockAdapter(RemoteStorage.API.axiosInstance);
}

export function mockRoute(params) {
  return {
    params: params || {}
  };
}

export function mockNavigation() {
  return {
    setOptions: () => {
    },
    addListener: () => {
    },
    setParams: (n, v) => {
    }
  };
}
