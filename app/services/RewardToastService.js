
export default class RewardToastService{

  static STORE = null;

  static setStoreForReward(store) {
      RewardToastService.STORE = store;
  }

  static showRewards(rewards) {
    RewardToastService.STORE.dispatch({type: 'SHOW_REWARDS', rewards});
  }

  static hideReward() {
    RewardToastService.STORE.dispatch({type: 'HIDE_REWARDS'});
  }

}