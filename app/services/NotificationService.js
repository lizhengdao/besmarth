import * as Notifications from "expo-notifications";
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";

// TODO: https://github.com/expo/expo/tree/master/packages/expo-notifications#dailynotificationtrigger
// TODO: daily/weekly/usw is android only, the same functionality will be achieved on iOS with a CalendarNotificationTrigger

Notifications.setNotificationHandler({
    handleNotification: async () => ({
        shouldShowAlert: true,
        shouldPlaySound: false,
        shouldSetBadge: false,
    }),
});

export async function scheduleDailyPushNotification(hour, minute, diaryData) {
    const identifier = await Notifications.scheduleNotificationAsync({
        content: {
            title: "Tägliche Selbstbeobachtung 🥳",
            body: "Wie ist es Dir heute mit Deinem Self-Commitment '" + diaryData.selectedSelfCommitment + "' ergangen?",
            data: {data: 'goes here'}, // TODO: was muss in data kommen?
        },
        trigger: {
            hour: hour,
            minute: minute,
            repeats: true
        }
    });
    return identifier
}

export async function scheduleWeeklyPushNotification(weekday, hour, minute, diaryData) {
    const dayBeforeActualDay = (weekday === 1 ? 7 : weekday-1) // ein tag vor startdatum
    const identifier = await Notifications.scheduleNotificationAsync({
        content: {
            title: "Wöchentlicher Statusbericht 🥳",
            body: "Wie ist es Dir diese Woche mit Deinem Self-Commitment '" + diaryData.selectedSelfCommitment + "' ergangen?",
            data: {data: 'goes here'}, // TODO: was muss in data kommen?
        },
        trigger: {
            // weekday von 1 bis 7, 1=Sunday, 7=Saturday
            weekday: dayBeforeActualDay,
            hour: hour,
            minute: minute,
            repeats: true
        }
    });
    alert(dayBeforeActualDay)
    return identifier
}

async function cancelPushNotification(identifier) {
    await Notifications.cancelScheduledNotificationAsync(identifier);
}

export async function registerForPushNotificationsAsync() {
    let token;
    if (Constants.isDevice) {
        const {status: existingStatus} = await Permissions.getAsync(Permissions.NOTIFICATIONS);
        let finalStatus = existingStatus;
        if (existingStatus !== 'granted') {
            const {status} = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
        }
        if (finalStatus !== 'granted') {
            alert('Failed to get push token for push notification!');
            return;
        }
        token = (await Notifications.getExpoPushTokenAsync()).data;
        console.log(token);
    } else {
        alert('Must use physical device for Push Notifications');
    }

    if (Platform.OS === 'android') {
        Notifications.setNotificationChannelAsync('default', {
            name: 'default',
            importance: Notifications.AndroidImportance.MAX,
            vibrationPattern: [0, 250, 250, 250],
            lightColor: '#FF231F7C',
        });
    }
    return token;
}