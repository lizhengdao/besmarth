import RemoteStorage from "./RemoteStorage";
import LocalStorage from "./LocalStorage";
import {notNull, toMap} from "./utils";
import mapItems from "../redux/reducers/topics";
import {isPhoneOnline} from "./phone-utils";

const TOPICS_API_PATH = "/topics/";
const TOPICS_CACHE_KEY = "topics";

/**
 * Fetches all topics from the server
 */
export function fetchTopics() {
  return async (dispatch, getState) => {
    dispatch({
      type: "RELOADING_TOPICS"
    });

    /**
     * Puts the Topics into the redux store.
     */
    function putIntoStore(dispatch, topics) {
      dispatch({
        type: "RECEIVED_TOPICS",
        topics: topics
      });
    }

    if (await isPhoneOnline()) {
      const response = await RemoteStorage.get(TOPICS_API_PATH);
      const topicMap = toMap(response.data.results, elem => elem.id);
      putIntoStore(dispatch, topicMap);
      await LocalStorage.setCacheItem(TOPICS_CACHE_KEY, getState().topics.topics);
    } else {
      console.log("Device is offline, getting challenges from cache");
      const cachedTopics = await LocalStorage.getCacheItem(TOPICS_CACHE_KEY) || [];
      if (notNull(mapItems())) {
        putIntoStore(cachedTopics);
      }
    }
  };

}

/**
 * Fetches the level of a topic
 */
export async function fetchLevel(topicId) {
  if (await isPhoneOnline()) {
    const response = await RemoteStorage.get(`/topics/${topicId}/progress`);
    return response.data;
  } else {
    throw new Error("device is offline and no topic-levels are cached");
  }
}