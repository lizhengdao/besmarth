import RemoteStorage from "./RemoteStorage";
import LocalStorage from "./LocalStorage";
import {notNull} from "./utils";
import {isPhoneOnline} from "./phone-utils";

const DIARIES_API_PATH = "/diary/"
export const DIARIES_CHACHE_KEY = "diaries"

export async function createDiary(diaryData) {
    const response = await RemoteStorage.post(DIARIES_API_PATH, diaryData);
    const diaries = response.data;
    return diaries;
}

export async function fetchDiaries() {
    if (await isPhoneOnline()) {
        const response = await RemoteStorage.get(DIARIES_API_PATH);
        const diaries = response.data.results;
        await LocalStorage.setCacheItem(DIARIES_CHACHE_KEY, diaries);
        return diaries;
    } else {
        console.log("Device is offline, getting challenges from cache");
        const cachedDiaries = await LocalStorage.getCacheItem(DIARIES_CHACHE_KEY);
        if (notNull(cachedDiaries)) {
            return cachedDiaries;
        } else {
            throw new Error("device is offline and no diaries are cached");
        }
    }
}

export async function updateDiary(diaryId, data) {
    const response = await RemoteStorage.patch(DIARIES_API_PATH + `${diaryId}/`, data);
    const diary = response.data;
    return diary;
}

export async function createPost(diaryId, postData) {
    const response = await RemoteStorage.post(DIARIES_API_PATH + `${diaryId}/posts/`, postData);
    const post = response.data;
    return post;
}

export async function createDailyMonitoring(diaryId, dailyMonitoringData) {
    const response = await RemoteStorage.post(DIARIES_API_PATH + `${diaryId}/dailyMonitorings/`, dailyMonitoringData);
    const dailyMonitoring = response.data;
    return dailyMonitoring;
}

export async function createFactor(diaryId, factorData) {
    const response = await RemoteStorage.post(DIARIES_API_PATH + `${diaryId}/factors/`, factorData);
    const factor = response.data;
    return factor;
}

export async function createWeeklyMonitoring(diaryId, weeklyMonitoringData) {
    const response = await RemoteStorage.post(DIARIES_API_PATH + `${diaryId}/weeklyMonitorings/`, weeklyMonitoringData);
    const weeklyMonitoring = response.data;
    return weeklyMonitoring;
}

export async function createHypothesis(diaryId, hypothesisData) {
    const response = await RemoteStorage.post(DIARIES_API_PATH + `${diaryId}/hypothesis/`, hypothesisData);
    const hypothesis = response.data;
    return hypothesis;
}

export async function createFinalMonitoring(diaryId) {
    const finalMonitoringData = {};
    const response = await RemoteStorage.post(DIARIES_API_PATH + `${diaryId}/finalMonitoring/`, finalMonitoringData);
    const finalMonitoring = response.data;
    return finalMonitoring;
}