import RemoteStorage from "./RemoteStorage";
import {emptyString, notNull} from "./utils";
import * as SecureStore from "expo-secure-store";
import {isPhoneOnline} from "./phone-utils";
import * as FileSystem from "expo-file-system";
import * as Sharing from "expo-sharing";
import React from "react";
import {Image} from "react-native";

export const ACCOUNT_API_PATH = "/account";
export const ACCOUNT_CACHE_KEY = "account";

function dispatchAccount(dispatch, account) {
  dispatch({
    type: "ACCOUNT_UPDATE",
    account: account
  });
}

export function updateAccount(account) {
  return async (dispatch, getState) => new Promise(async (resolve, reject) => {
    dispatch({type: "ACCOUNT_PENDING"});

    try {
      if (emptyString(account.password)) {
        account.password = undefined;
      }
      const result = await RemoteStorage.patch(ACCOUNT_API_PATH, account);
      dispatch({type: "ACCOUNT_UPDATE", account: result.data});
      resolve(result.data);
    } catch (e) {
      reject(e.data);
    }
  });
}

/**
 * Loads account data from server
 * @returns {Function}
 */
export function loadAccount() {
  return async (dispatch, getState) => {
    dispatch({
      type: "ACCOUNT_PENDING"
    });

    // Fetch account from backend when device has connection
    if (await isPhoneOnline()) {
      try {
        const response = await RemoteStorage.get(ACCOUNT_API_PATH);
        dispatchAccount(dispatch, response.data);
      } catch (e) {
        console.log(e);
        throw new Error("Accountinformationen aktuell nicht verfügbar.");
      }
    } else {
      const cachedAccount = JSON.parse(await SecureStore.getItemAsync(ACCOUNT_CACHE_KEY));
      if (notNull(cachedAccount)) {
        dispatchAccount(dispatch, cachedAccount);
      } else {
        throw new Error("Accountinformationen aktuell nicht verfügbar.");
      }
    }
  };
}

export function deleteAccount(account) {
  return async (dispatch, getState) => {
    try {
      await RemoteStorage.delete(ACCOUNT_API_PATH);
    } catch (e) {
      throw new Error("Accountinformationen aktuell nicht verfügbar.");
    }
  };
}


/**
 * Downloads the account data (export) into a file and returns the file url.
 * @returns {Promise<unknown>}
 */
export async function downloadAndOpenAccountData() {
  const fileUrl = FileSystem.documentDirectory + "besmarth-data.json";
  await FileSystem.downloadAsync(RemoteStorage.getBaseUrl() + ACCOUNT_API_PATH + "/data", fileUrl, {
    headers: {
      "Authorization": RemoteStorage.API.headers["Authorization"]
    }
  });
  await Sharing.shareAsync(fileUrl);
}

export function getAccountCodeImage(username, width, height) {
  return <Image source={{
    method: "GET",
    headers: {
      Authorization: RemoteStorage.API.headers["Authorization"]
    },
    // the username is only passed to invalidate cache if username changes
    uri: RemoteStorage.getBaseUrl() + ACCOUNT_API_PATH + "/code.png?username=" + username
  }} style={{width, height}}/>;
}