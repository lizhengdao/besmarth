import {emptyString, getDateTime, getTodayDate, notNull} from "../utils";
import MockDate from "mockdate";

describe(`utils`, () => {
  it(`notNull`, () => {
    expect(notNull(null)).toBe(false);
    expect(notNull(undefined)).toBe(false);
    expect(notNull("1")).toBe(true);
    expect(notNull(-1)).toBe(true);
    expect(notNull("")).toBe(true);
  });

  it(`emptyString`, () => {
    expect(emptyString("")).toBe(true);
    expect(emptyString(" ")).toBe(true);
    expect(emptyString("1")).toBe(false);
    expect(emptyString("22")).toBe(false);
    expect(emptyString("a")).toBe(false);
  });

  it("getTodayDate", () => {
    MockDate.set('2019-05-14');
    const date2 = new Date("2019-05-14T00:00:00.000Z");
    expect(getTodayDate()).toEqual(date2);
  });

  it("getDateTime", () => {
    MockDate.set('2019-05-14T11:01:58.135Z');
    expect(getDateTime()).toEqual(new Date("2019-05-14T11:01:58.135Z"));
  });
});