import {applyMiddleware, createStore} from "redux";
import rootReducer from "../redux/rootReducer";
import thunkMiddleware from "redux-thunk";
import reduxUnhandledAction from "redux-unhandled-action";
import {notNull} from "../services/utils";

export default function initStore(initialState) {
  const callback = (action) => {
    console.error("The redux action '" + action.type + "' was issued, but the redux state remained unchanged.");
  };

  const middleware = applyMiddleware(thunkMiddleware, reduxUnhandledAction(callback));
  if(notNull(initialState)){
     return createStore(rootReducer, initialState, middleware);
  } else {
   return createStore(rootReducer, middleware);
  }
};