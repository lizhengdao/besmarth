import {combineReducers} from "redux";
import dailyTip from './reducers/dailyTip';
import account from './reducers/account';
import app from './reducers/app';
import topics from './reducers/topics'

export default combineReducers({
  app,
  dailyTip,
  account,
  topics,
});