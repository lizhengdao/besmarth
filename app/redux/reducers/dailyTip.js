const initial = {
  date: '1990-10-05T22:19:34.550Z',
  title: 'Standard',
  content: 'Kein Tipp des Tages gefunden.'
};

export default function dailyTip(state = initial, action) {
  switch (action.type) {
    case 'RECEIVE_DAILY_TIP':
      return action.dailyTip;
    case 'ERROR_DAILY_TIP':
      return {error: true};
    default:
      return state;
  }
}