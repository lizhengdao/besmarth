import {createEmptyTestStore, createNetworkMock, renderForTest} from "../../test/test-utils";
import React from "react";
import NavigationRoot from "../NavigationRoot";
import {waitForElementToBeRemoved} from "@testing-library/react-native";

const mock = createNetworkMock();

describe("NavigationRoot", () => {
  it(`renders`, async () => {
    mock.reset();
    mock.onPost("/account").reply(200, {
      username: "testuser",
      firstName: "Samuel",
      email: "test@email.com",
      token: "mytoken"
    });
    const {getByTestId, asJSON} = renderForTest(<NavigationRoot/>, createEmptyTestStore());
    await waitForElementToBeRemoved(() => getByTestId("nav-loading-indicator"));
    expect(asJSON()).toMatchSnapshot();
  });
  it(`renders in devmode`, async () => {
    try {
      window.__TESTMODE__ = false;
      mock.reset();
      mock.onGet("/account/devtoken").reply(200, "mytoken");
      mock.onGet("/account").reply(200, {
        username: "testuser",
        firstName: "Samuel",
        email: "test@email.com",
        token: "mytoken"
      });

      const {getByTestId, asJSON} = renderForTest(<NavigationRoot/>, createEmptyTestStore());
      await waitForElementToBeRemoved(() => getByTestId("nav-loading-indicator"));
      expect(asJSON()).toMatchSnapshot();
    } finally {
      window.__TESTMODE__ = true;
    }
  });
});