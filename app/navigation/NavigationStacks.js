import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import HomeScreen from "../screens/HomeScreen";
import DailyTipScreen from "../screens/DailyTipScreen";
import TopicScreen from "../screens/TopicScreen";
import ChallengesScreen from "../screens/ChallengesScreen";
import ChallengeDetailScreen from "../screens/ChallengeDetailScreen";
import MyChallengesScreen from "../screens/MyChallengesScreen";
import FriendsScreen from "../screens/Friends/FriendsScreen";
import FriendDetailScreen from "../screens/FriendDetailScreen";
import ManageAccountScreen from "../screens/Account/ManageAccountScreen";
import SignInScreen from "../screens/Account/SignInScreen";
import SignUpScreen from "../screens/Account/SignUpScreen";
import AccountCodeScreen from "../screens/Friends/AccountCodeScreen";
import ScanFriendCodeScreen from "../screens/Friends/ScanFriendCodeScreen";
import RewardScreen from "../screens/RewardScreen";
import InfotrailerScreen from "../screens/InfotrailerScreen";
import ChallengeReasonScreen from "../screens/ChallengeReasonScreen";
import ChallengeHypothesisScreen from "../screens/ChallengeHypothesisScreen";
import ChallengePushSettingScreen from "../screens/ChallengePushSettingScreen";
import DailyObservationScreen from "../screens/DailyObservationScreen";
import DailyReflectionScreen from "../screens/DailyReflectionScreen";
import WeeklyExperienceReportScreen from "../screens/WeeklyExperienceReportScreen";
import WeeklyFactorsOverviewScreen from "../screens/WeeklyFactorsOverviewScreen";
import WeeklyActionsScreen from "../screens/WeeklyActionsScreen";
import ExperienceOverviewScreen from "../screens/ExperienceOverviewScreen";
import FinalReportScreen from "../screens/FinalReportScreen";
import FinalReportFactorsScreen from "../screens/FinalReportFactorsScreen";
import AddChallengeScreen from "../screens/AddChallengeScreen";
import FinalReportIntroScreen from "../screens/FinalReportIntroScreen";

const HomeStack = createStackNavigator();

export function HomeStackScreen() {
    return (
        <HomeStack.Navigator initialRouteName="Home">
            <HomeStack.Screen name="Home" component={HomeScreen} options={{headerShown: false}}/>
            <HomeStack.Screen name="DailyTip" component={DailyTipScreen}
                              options={{headerShown: false, title: "Tipp des Tages"}}/>
            <HomeStack.Screen name="TopicDetail" component={TopicScreen}/>
            <HomeStack.Screen name="Challenges" component={ChallengesScreen}/>
            <HomeStack.Screen name="AddChallenge" component={AddChallengeScreen}
                              options={{title: "Challenge erstellen"}}/>
            <HomeStack.Screen name="ChallengeDetail" component={ChallengeDetailScreen}/>
            <HomeStack.Screen name="Infotrailer" component={InfotrailerScreen}
                              options={{title: "Infotrailer Tagebuch"}}/>
            <HomeStack.Screen name="ChallengeReason" component={ChallengeReasonScreen}
                              options={{title: "Mein Self-Commitment"}}/>
            <HomeStack.Screen name="ChallengeHypothesis" component={ChallengeHypothesisScreen}
                              options={{title: "Mein Self-Commitment"}}/>
            <HomeStack.Screen name="ChallengePushSetting" component={ChallengePushSettingScreen}
                              options={{title: "Push Zeitpunkt"}}/>
        </HomeStack.Navigator>
    );
}

const MyChallengesStack = createStackNavigator();

export function MyChallengesStackScreen() {
    return (
        <MyChallengesStack.Navigator initialRouteName="MyChallenges">
            <MyChallengesStack.Screen name="MyChallenges" component={MyChallengesScreen}
                                      options={{title: "Deine Challenges"}}/>
            <MyChallengesStack.Screen name="ChallengeDetail" component={ChallengeDetailScreen}
                                      options={{title: "Challenge"}}/>
        </MyChallengesStack.Navigator>
    );
}

const FriendsStack = createStackNavigator();

export function FriendsStackScreen() {
    return (
        <FriendsStack.Navigator initialRouteName="Friends">
            <FriendsStack.Screen name="Friends" component={FriendsScreen} options={{title: "Freunde"}}/>
            <FriendsStack.Screen name="FriendDetail" component={FriendDetailScreen} options={{title: "Freund"}}/>
            <FriendsStack.Screen name="AccountCode" component={AccountCodeScreen} options={{title: "Dein Code"}}/>
            <FriendsStack.Screen name="ScanFriendCode" component={ScanFriendCodeScreen}
                                 options={{title: "Code scannen"}}/>
            <FriendsStack.Screen name="ChallengeDetail" component={ChallengeDetailScreen}
                                 options={{title: "Challenge"}}/>
        </FriendsStack.Navigator>
    );
}

const AccountStack = createStackNavigator();

export function AccountStackScreen() {
    return (
        <AccountStack.Navigator initialRouteName="ManageAccount">
            <AccountStack.Screen name="ManageAccount" component={ManageAccountScreen}
                                 options={{title: "Accountverwaltung"}}/>
            <AccountStack.Screen name="SignIn" component={SignInScreen} options={{title: "Sign In"}}/>
            <AccountStack.Screen name="SignUp" component={SignUpScreen} options={{title: "Sign Up"}}/>
            <AccountStack.Screen name="Rewards" component={RewardScreen} options={{title: "Rewards"}}/>
        </AccountStack.Navigator>
    );
}

const DiaryStack = createStackNavigator();

/* TODO: Wenn trailer akzeptiert ist wird immer Erfahrungsübersicht angezeigt*/
export function DiaryStackScreen() {
    return (
        <DiaryStack.Navigator initialRouteName="ExperienceOverview">
            <DiaryStack.Screen name="ExperienceOverview" component={ExperienceOverviewScreen}
                               options={{title: "Erfahrungsübersicht"}}/>
            <DiaryStack.Screen name="DailyObservation" component={DailyObservationScreen}
                               options={{title: "Tägliche Selbstbeobachtung"}}/>
            <DiaryStack.Screen name="DailyReflection" component={DailyReflectionScreen}
                               options={{title: "Tägliche Selbstreflexion"}}/>
            <DiaryStack.Screen name="WeeklyExperienceReport" component={WeeklyExperienceReportScreen}
                               options={{title: "Wöchentliche Erfahrung"}}/>
            <DiaryStack.Screen name="WeeklyFactorsOverview" component={WeeklyFactorsOverviewScreen}
                               options={{title: "Wöchentliche Faktoren"}}/>
            <DiaryStack.Screen name="WeeklyActions" component={WeeklyActionsScreen}
                               options={{title: "Wöchentliche Massnahmen"}}/>
            <DiaryStack.Screen name="FinalReportIntro" component={FinalReportIntroScreen}
                               options={{title: "Schlussbericht Einleitung"}}/>
            <DiaryStack.Screen name="FinalReport" component={FinalReportScreen} options={{title: "Schlussbericht"}}/>
            <DiaryStack.Screen name="FinalReportFactors" component={FinalReportFactorsScreen}
                               options={{title: "Schlussbericht Faktoren"}}/>
        </DiaryStack.Navigator>
    );
}