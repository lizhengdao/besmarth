import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Navigation from "../services/Navigation";
import {Card} from "react-native-paper";
import {StyleSheet, View} from "react-native";
import {notNull} from "../services/utils";
import {Ionicons} from "@expo/vector-icons";

class ChallengeListItem extends React.Component {

  renderTitleRight() {
    return (<Ionicons style={styles.rightIcon} name={"ios-arrow-forward"} size={20}/>);
  }

  render() {
    const challenge = this.props.challenge;

    const image = notNull(challenge.image) ? {uri: challenge.image} : require("../assets/images/no-challenge-image.png");

    return (
      <View accessibilityLabel={challenge.title}>
        <Card style={styles.challengeCard} onPress={this.onSelect.bind(this)}>
          <Card.Cover source={image} style={styles.challengeImage}/>
          <Card.Title style={[{backgroundColor: challenge.color}]} title={challenge.title}
                      right={this.renderTitleRight.bind(this)}/>
        </Card>
      </View>);
  }

  /**
   * Handles selection of a challenge
   */
  onSelect() {
    Navigation.push("HomeTab", "ChallengeDetail", {challenge: this.props.challenge});
  }
}

const styles = StyleSheet.create({
  challengeCard: {
    margin: 8
  },
  challengeImage: {
    height: 150,
    width: "100%",
  },
  rightIcon: {
    marginRight: 10
  }
});

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ChallengeListItem);