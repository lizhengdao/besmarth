import React from "react";
import {Picker, StyleSheet, TextInput, View} from "react-native";
import {Button, Text} from "react-native-paper";

export class ChallengesFilter extends React.Component {
  state = {
    category: null,
    difficulty: null,
    query: null
  };

  resetFilter() {
    this.setState({
      category: null,
      difficulty: null,
      query: null
    }, () => {
      this.props.onFilter(this.state);
    });
  }

  render() {
    const handler = (propName, lowercase = false) => (value) => {
      this.setState({
        ...this.state,
        [propName]: lowercase ? value.toLowerCase() : value
      }, () => {
        this.props.onFilter(this.state);
      });
    };

    if (this.props.visible) {
      return <View style={styles.container}>
        <Text>Nach Challenges suchen</Text>
        <TextInput style={styles.queryField} value={this.state.query}
                   accessibilityLabel="Suchbegriff"
                   onChangeText={handler("query", true)}
                   placeholder="Suchen..."/>
        <Text>Kategorie</Text>
        <Picker selectedValue={this.state.category} onValueChange={handler("category")}
                prompt="Nach Kategorie filtern">
          <Picker.Item label="Alle" value={null}/>
          <Picker.Item label="Mobilität" value="MOBILITY"/>
          <Picker.Item label="Shopping" value="SHOPPING"/>
          <Picker.Item label="Leben" value="LIFE"/>
          <Picker.Item label="Arbeit/Ausbildung" value="WORK_EDU"/>
        </Picker>
        <Button onPress={this.resetFilter.bind(this)} accessibilityLabel="Filter zurücksetzen" compact>Filter zurücksetzen</Button>
      </View>;
    } else {
      return null;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 6,
    backgroundColor: "#F8F9FA"
  },
  queryField: {
    marginHorizontal: 6,
    padding: 8
  }
});
