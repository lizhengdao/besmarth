import React from 'react';
import {StyleSheet, View, Text} from "react-native";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {faMedal, faTrophy, faAward} from "@fortawesome/free-solid-svg-icons";


export class RewardItem extends React.Component{

  constructor(props) {
    super(props);
  }

  render() {
    const reward = this.props?.reward || {};
    const fontSize = 50;
    let rewardView;

    switch(reward.type){
      case "MEDAL":
        rewardView = (
          <View style={styles.box}>
            <FontAwesomeIcon icon={faMedal} style={styles.reward} size={fontSize} color={reward.color} />
            <Text stlye={styles.text}>({reward.points})</Text>
            <Text style={styles.text}>Du hast {reward.condition} Challenges in einer Kategorie erledigt</Text>
          </View>
        );
        break;

      case "TROPHY":
        rewardView = (
          <View  style={styles.box}>
            <FontAwesomeIcon icon={faTrophy} style={styles.reward} size={fontSize} color={reward.color}/>
            <Text stlye={styles.text}>({reward.points})</Text>
            <Text style={styles.text}>Du hast {reward.condition} Challenges in allen Kategorien erledigt</Text>
          </View>
        );
        break;

      case "BADGE":
        rewardView = (
          <View  style={styles.box}>
            <FontAwesomeIcon icon={faAward} style={styles.reward} size={fontSize} color={reward.color}/>
            <Text stlye={styles.text}>({reward.points})</Text>
          </View>
        );
        break;
    }

    return rewardView;
  }

}
const styles = StyleSheet.create({
    text:{
      flex: 1,
      flexDirection: "row"
    },

    box : {
      flex: 1,
      flexDirection: "row",
      alignItems: "center"
    }
});
