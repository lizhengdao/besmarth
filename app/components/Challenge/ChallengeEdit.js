import React from "react";
import {StyleSheet, View, Text} from "react-native";
import MotionSlider from 'react-native-motion-slider';
import {Divider} from "react-native-paper";

export class ChallengeEdit extends React.Component {

    constructor(props) {
        super(props);
    }

    durationHandler = value => {
        this.props.onChallengeDurationChange(value);
    }

    periodicityToUnitString(periodicity) {
        switch (periodicity.toLowerCase()) {
        case "daily":
            return "Tage";
        case "weekly":
            return "Wochen";
         case "monthly":
          return "Monate";
         default:
         return "<Unknown periodicity " + periodicity;
        }
     }

    render() {

        const challenge = this.props.challenge;

        return <View>
            <Text style={styles.text}>Challenge Dauer einstellen:</Text>
            <View style={styles.editWrapper}>
                <MotionSlider
                    titleColor={"rgb(0, 0, 0)"}
                    minColor={"rgb(255,255,255)"}
                    maxColor={"rgb(255,255,255)"}
                    valueBackgroundColor={"rgb(255, 255, 255)"}
                    backgroundColor={["rgb(0, 107, 42)"]}
                    min={1}
                    max={30}
                    value={challenge.duration}
                    onValueChanged={value => this.durationHandler(value)}
                    units={" " + this.periodicityToUnitString(challenge.periodicity)}
                    borderRadius={40}
                />
            </View>
            <Divider />
        </View>
    }
}

const styles = StyleSheet.create({
    editWrapper: {
        alignItems: "center",
        paddingBottom: 10,
        paddingTop: 10
    },
    text: {
        paddingBottom: 20,
        paddingTop: 25,
        paddingStart: 10
    }
});