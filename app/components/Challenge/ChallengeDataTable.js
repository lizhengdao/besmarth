import {DataTable} from "react-native-paper";
import {StyleSheet, View} from "react-native";
import React from "react";

/**
 * Displays difficulty and duration about a challenge in a data table.
 * Is not connected with the redux store
 * Use like <ChallengeDataTable challenge={yourChallenge} />
 */
export class ChallengeDataTable extends React.Component {

  periodicityToString(periodicity) {
    switch (periodicity.toLowerCase()) {
      case "daily":
        return "Täglich";
      case "weekly":
        return "Wöchentlich";
      case "monthly":
        return "Monatlich";
      default:
        return "<Unknown periodicity " + periodicity;
    }
  }

  periodicityToUnitString(periodicity) {
    switch (periodicity.toLowerCase()) {
      case "daily":
        return "Tage";
      case "weekly":
        return "Wochen";
      case "monthly":
        return "Monate";
      default:
        return "<Unknown periodicity " + periodicity;
    }
  }

    render() {

        const challenge = this.props.challenge;

        return <View>
            <DataTable>
                <DataTable.Header>
                    <DataTable.Title numeric>Frequenz</DataTable.Title>
                    <DataTable.Title numeric>Dauer</DataTable.Title>
                </DataTable.Header>

                <DataTable.Row>
                    <DataTable.Cell numeric>{this.periodicityToString(challenge.periodicity)}</DataTable.Cell>
                    <DataTable.Cell numeric>{challenge.duration} {this.periodicityToUnitString(challenge.periodicity)}</DataTable.Cell>
                </DataTable.Row>
            </DataTable>
        </View>
    }

}

const styles = StyleSheet.create({

});