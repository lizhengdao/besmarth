import {StyleSheet, View} from "react-native";
import Speedometer from "react-native-speedometer-chart";
import React from "react";
import {Paragraph} from "react-native-paper";
import {Colors, Typography} from  "styles/index";



/**
 * Shows progress information about the challenge
 *
 * Use like <ChallengeProgress participation={yourParticipation} challenge={yourChallenge} />
 */
export class ChallengeProgress extends React.Component {

  static countCompletedProgress(countCompletedProgress) {
    if (countCompletedProgress !== 0) {
      return <Paragraph style={Typography.challengeInfo}>Du hast diese Challenge bereits <Paragraph
        style={{fontWeight: "bold"}}>{countCompletedProgress}</Paragraph> Mal abgeschlossen.</Paragraph>;
    } else {
      return <Paragraph style={Typography.challengeInfo}>Noch nicht abgeschlossen</Paragraph>;
    }
  }


  render() {
    const challenge = this.props.challenge;
    const participation = this.props.participation;
    const achievedCount = participation?.progress?.length || 0;
    const challengeDuration = challenge.duration;
    const countCurrentProgress = (achievedCount % challengeDuration);

    return (
      <View>
        <Speedometer style={styles.progress}
                     value={countCurrentProgress}
                     totalValue={challengeDuration}
                     size={250}
                     outerColor="#86bfc2"
                     internalColor="#1a898f"
                     showText
                     text={countCurrentProgress + "/" + challengeDuration}
                     textStyle={{
                       color: Colors.gray,
                       fontSize: 16,
                       fontWeight: "bold"
                     }}
                     showPercent
                     percentStyle={{
                       color: Colors.darkGray,
                       fontSize: 24,
                       fontWeight: "bold"
                     }}
        />

      </View>);
  }
}

const styles = StyleSheet.create({
  progress: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
});
