import {StyleSheet, Text, View} from "react-native";
import React from "react";
import {ChallengeProgress} from "./ChallengeProgress";
import {Card, FAB} from "react-native-paper";
import Navigation from "../../services/Navigation";
import {Colors} from  "../../styles/index";

/**
 * Shows progress information about shared challenge with title and number of times completed
 */
export class FriendChallengeProgress extends React.Component {
  buildTimesCompleted() {
    const achievedCount = this.props.participation?.progress?.length || 0;
    const duration = this.props.participation.challenge.duration;
    const numberOfTimesCompleted = Math.floor(achievedCount / duration);

    if (numberOfTimesCompleted < 1) return null;

    return <Text style={{marginBottom: 20}}>{numberOfTimesCompleted} Mal durchgeführt</Text>;
  }

  render() {
    return (
      <View>
        <Card>
          <Card.Title title={this.props.participation.challenge.title}/>
          <Card.Content>
            {this.buildTimesCompleted()}
            <ChallengeProgress participation={this.props.participation}
                               challenge={this.props.participation.challenge}></ChallengeProgress>
            <FAB style={{marginTop: 20, backgroundColor: Colors.green}} onPress={this.onSelect.bind(this)} label={"Zu deiner Challenge"}/>
          </Card.Content>
        </Card>
      </View>
    );
  }


  /**
   * Handles selection of a challenge
   */
  onSelect() {
    Navigation.push("FriendsTab", "ChallengeDetail", {challenge: this.props.participation.challenge});
  }
}

const styles = StyleSheet.create({
  challengeProgress: {
    position: "relative",
    margin: 30
  },
});