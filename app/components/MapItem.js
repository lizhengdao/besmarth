import React from "react";
import {Image, StyleSheet, View} from "react-native";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Navigation from "../services/Navigation";
import {FAB, Headline, Surface, TouchableRipple} from "react-native-paper";
import {Colors} from  "../styles/index";

class MapItem extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  render() {
    const image = this.props.topic.image_level1;
    return (
      <TouchableRipple accessibilityLabel="Bild: Jetzt retten"
                       onPress={() => this.navigateToChallenges()}>
        <View style={styles.mapItem}>
          <Image source={{uri: image}} style={styles.mapImage}/>
          <Surface style={styles.topicOverlay}>
            <Headline style={styles.topicTitle}>{this.props.topic.topic_name}</Headline>
            <FAB style={{backgroundColor: Colors.green, marginVertical: 10, marginHorizontal: 30}}
                 label="Jetzt retten" accessibilityLabel="Jetzt retten"
                 onPress={() => this.navigateToChallenges()}/>
          </Surface>
        </View>
      </TouchableRipple>
    );
  }

  navigateToMapItem() {
    this.props.changeCurrentTopic(this.props.topic);
  }

  // TODO: Topic Name wird zu spät gesetzt und wird erst beim 2. mal draufklicken korrekt gesetzt
  navigateToChallenges() {
    this.props.changeCurrentTopic(this.props.topic);
    //Navigation.navigate("HomeTab", "TopicDetail");
    Navigation.push("HomeTab", "Challenges", {topicName: this.props.currentTopicName});
  }
}

const styles = StyleSheet.create({
  mapItem: {
    height: "100%",
    width: "100%",
    position: "relative"
  },
  mapImage: {
    height: "100%",
    width: "100%",
    backgroundColor: "black"
  },
  topicOverlay: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    paddingHorizontal: 10,
    paddingVertical: 10,
    paddingBottom: 90,
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "rgba(0,0,0,0.3)",
  },
  topicTitle: {
    color: "white",
    marginBottom: 10,
  },
  level: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    paddingTop: 40,
    backgroundColor: "rgba(0,0,0,0.3)",
  },
});

const mapStateToProps = state => {
  return {
    currentTopicId: state.topics.currentTopicId,
    currentTopicName: state.topics.currentTopicName,
    currentTopicDescription: state.topics.currentTopicDescription,
    currentTopicImage: state.topics.currentTopicImage
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  changeCurrentTopic: (item) => (dispatch2) => dispatch2({type: "CHANGE_CURRENT_TOPIC", item})
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MapItem);