import React from "react";
import {Ionicons} from "@expo/vector-icons";
import theme from "../themes/theme";

export default function TabBarIcon(props) {
  return (
    <Ionicons
      name={props.name}
      size={25}
      style={{marginBottom: 0}}
      color={props.focused ? theme.colors.primary : '#cccccc'}
    />
  );
}
