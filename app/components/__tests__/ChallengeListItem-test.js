import React from "react";
import ChallengeListItem from "../ChallengeListItem";
import {renderForSnapshot} from "../../test/test-utils";

describe("ChallengeListItem", () => {

  it(`renders`, () => {
    const tree = renderForSnapshot(<ChallengeListItem challenge={{
      id: 134515,
      title: "Challenge Title",
      image: "http://test.local/image/url",
      color: "#abcdef",
      periodicity: "daily",
      duration: 100
    }}/>).toJSON();
    expect(tree).toMatchSnapshot();
  });
});