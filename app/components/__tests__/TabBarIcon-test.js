import {renderForSnapshot} from "../../test/test-utils";
import React from "react";
import TabBarIcon from "../TabBarIcon";

describe("TabBarIcon", () => {
  it(`renders`, () => {
    const tree = renderForSnapshot(<TabBarIcon focused={true} name="ios-contact"/>).toJSON();
    expect(tree).toMatchSnapshot();
  });
});